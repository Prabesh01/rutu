import requests
import json
import sys
import os
from dotenv import load_dotenv
load_dotenv()
ghurl = os.getenv("Github_gist_URL")
ghheader = {'Authorization': f'token {os.getenv("gist_token")}'}

rb= json.loads(requests.get(ghurl).json()['files']['blob.json']['content'])
hooks=rb['hooks']
hookslen=len(hooks)
chanb=rb['channel']

hokz=[]
chan=[]
guil=[]
for idx, hook in enumerate(hooks):
    print(f'{idx}/{hookslen}')
    id=requests.get(hook)
    if id.status_code not in [200, 404]: sys.exit()
    id=id.json()
    try:
        guu=id['guild_id']
        if guu in guil: continue
        guil.append(guu)
        channal=id['channel_id']
        chan.append(channal)
        hokz.append(hook)
    except Exception as  e:
        print(e)

data = {"hooks": hokz, "channel": chan}
requests.patch(ghurl, data=json.dumps({'files': {'blob.json': {"content": json.dumps(data)}}}),
               headers=ghheader)
print('done')
