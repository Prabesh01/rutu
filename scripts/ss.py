import requests
from bs4 import BeautifulSoup
import re

ss={}

page=requests.get('https://hamropatro.com/news')
soup = BeautifulSoup(page.content, "html.parser")
listdiv=soup.findAll("div", attrs={"style":"margin-bottom: 30px"})
sources=listdiv[1].find_all("div")[1].findAll("a")
for source in sources:
    site=re.findall('(?<=ss=).*?(?=&)', source.attrs['href'])[0]
    img=re.findall('(?<=/)https?://.*?(?=")', str(source))[0]
    name=source.find('span').text
    ss[site.lower()]=[name,img]
ss['hamropatro.com/news']=['Hamro Patro','https://www.hamropatro.com/images/hamropatro.png']
print(ss)
