import requests
import json
import os
import nepali_datetime

eventz={}

r=requests.get(f"https://miti.bikram.io/data/{nepali_datetime.date.today().year}-calendar.json").json()

for month in r.keys():
    print(month)
    for day in r[month]:
        date=day["AD_date"]["bs"].split('.')
        date=f"{date[2]}-{date[1]}-{date[0]}"
        events=day["events"]
        totalev=len(events)
        if totalev==0: continue
        elif totalev==1:
            if events[0]["jds"]:
                eventz[date]=events[0]["jds"]["en"]
        else:
            for idx, ev in enumerate(events):
                if idx==0:
                    eventz[date]=ev["jds"]["en"]
                else:
                    eventz[date] += ' / '+ev["jds"]["en"]

with open(os.path.dirname(os.path.abspath(__file__))+'/../out/cal.json', 'w') as xdu:
    json.dump(eventz,xdu)
