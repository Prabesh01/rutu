import requests
import os
from PIL import Image, ImageDraw,ImageFont
import datetime
import pytz

tz_NP = pytz.timezone('Asia/Kathmandu')

now = datetime.datetime.now(tz_NP)
print(now)
basedir=os.path.dirname(os.path.abspath(__file__))+'/../'
fontpath=basedir+'/assets/font.ttf'

daz=requests.get('https://portal.edcd.gov.np/covid19/dataservice/data-dev.php', headers={'Referer': 'https://portal.edcd.gov.np/covid19/'}, verify=False).json()
try:
    daz=daz['today']
except:
    daz=''
if daz=='':
    daz=now.strftime('%Y-%m-%d')

print(daz)

ok=requests.get('https://portal.edcd.gov.np/covid19/dataservice/data-dev.php?sDate=2020-01-01&eDate='+daz+'&disease=COVID-19', headers={'Referer': 'https://portal.edcd.gov.np/covid19/'}, verify=False).json()
case=0
for i in range(len(ok)):
    case+=int(ok[i]['Value'])

os=requests.get('https://portal.edcd.gov.np/covid19/dataservice/data-dev.php?type=day&sDate=2020-01-01&eDate='+daz+'&disease=COVID-19', headers={'Referer': 'https://portal.edcd.gov.np/covid19/'}, verify=False).json()
ncase=0
for i in range(len(os)):
    ncase+=int(os[i]['Value'])

oo=requests.get('https://portal.edcd.gov.np/covid19/dataservice/data-dev.php?type=outcome&sDate=2020-01-01&eDate='+daz+'&disease=COVID-19', headers={'Referer': 'https://portal.edcd.gov.np/covid19/'}, verify=False).json()
acase=0
recover=0
death=0
for i in range(len(oo)):
    recover += float(oo[i]['Number of cases recovered']) * int(oo[i]['Value'])
    death += float(oo[i]['Number of deaths']) * int(oo[i]['Value'])
acase = int(case - recover -death)
recover=int(recover)
death=int(death)

ol=requests.get('https://portal.edcd.gov.np/covid19/dataservice/data-dev.php?type=outcomeDay&sDate=2020-01-01&eDate='+daz+'&disease=COVID-19', headers={'Referer': 'https://portal.edcd.gov.np/covid19/'}, verify=False).json()
nrecover=0
ndeath=0
try:
    nrecover=float(ol[0]['Number of cases recovered'])+0
except:
    pass

nrecover=int(nrecover)
ndeath=0
try:
    ndeath=float(ol[0]['Number of deaths'])+0
except: pass
ndeath=int(ndeath)

back_bok = Image.open(basedir+'/assets/covid.png')

back_bok = back_bok.copy()
d1 = ImageDraw.Draw(back_bok)
d1.text((100, 48), daz, fill=(0, 0, 0), font=ImageFont.truetype(fontpath, size=10))
d1.text((210, 169),str(ncase), fill=(0, 0, 0), font=ImageFont.truetype(fontpath, size=20))
d1.text((380, 169),str(nrecover), fill=(0, 0, 0), font=ImageFont.truetype(fontpath, size=20))
d1.text((570, 169),str(ndeath), fill=(0, 0, 0), font=ImageFont.truetype(fontpath, size=20))
d1.text((112, 363),str(case), fill=(0, 0, 0), font=ImageFont.truetype(fontpath, size=20))
d1.text((280, 363),str(acase), fill=(0, 0, 0), font=ImageFont.truetype(fontpath, size=20))
d1.text((470, 363),str(recover), fill=(0, 0, 0), font=ImageFont.truetype(fontpath, size=20))
d1.text((670, 363),str(death), fill=(0, 0, 0), font=ImageFont.truetype(fontpath, size=20))

back_bok.save(basedir+'/out/19.png')

print('\n#########################################\n')