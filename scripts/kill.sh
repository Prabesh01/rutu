date
kill -9 $(pgrep -fl '/rutu/bot.py' | sed 's/ .*//')
nohup python3 -u ~/rutu/bot.py > ~/botlogs/bot.log 2>&1 &

if [ "$(pgrep -fl server.bot.py)" ];
then
    echo "running"
else
    echo "needs to be run"
    nohup python3 -u ~/rutu/server.bot.py 1 > ~/botlogs/server.bot.log 2>&1 &
    # nohup ~/rutu/scraptest/scrape.sh > ~/botlogs/scrape.log 2>&1 &
fi
