import requests
from bs4 import BeautifulSoup
import urllib.request
import audioread
import os, shutil, subprocess
import re
import json
from pytube import Playlist, YouTube

opener = urllib.request.build_opener()
opener.addheaders = [('User-agent', 'Mozilla/5.0')]
urllib.request.install_opener(opener)

current_dir = os.path.dirname(os.path.abspath(__file__))
basedir = os.path.abspath(os.path.join(current_dir, os.pardir))
outdir = os.path.abspath(os.path.join(basedir, 'out'))


def get_min(link):
    response = requests.head(link, allow_redirects=True)
    urllib.request.urlretrieve(response.url, outdir+'/audio.mp3')
    with audioread.audio_open(outdir+'/audio.mp3') as f:
        duration_in_minutes = int(f.duration // 60)
    os.remove(outdir+'/audio.mp3')
    return response.url, duration_in_minutes


finaldata = {}

# https://tunein.com/podcasts/News--Politics-Podcasts/Nepal-Now-p1342569/
# https://tunein.com/podcasts/Radio-Drama-Podcasts/The-Orbiting-Human-Circus-p952586/
toprocess={"nepal_now":"p1342569", "orbitinghumancircus":"p952586"}

for podcast in toprocess:
    finaldata[podcast]=[]

    page=requests.get(f'https://api.tunein.com/profiles/{toprocess[podcast]}/contents?filter=t:free&offset=0&limit=100').json()

    for item in page['Items']:
        link=requests.get('https://opml.radiotime.com/Tune.ashx?id='+item['GuideId']).text.strip().replace('https://pdcn.co/e/www.','https://')

        title = item['Title'].replace('\u2014', '-')
        length = int(item['Actions']['Play']['Duration'])//60

        finaldata[podcast].append({title:[link,int(length)]})

    finaldata[podcast].reverse()
    print(podcast)
    print('----------------')

# inconsideration=['https://hamroawaz.blogspot.com/2016/03/kathmandu-selfie-prakash-saayami.html','http://hamroawaz.blogspot.com/2014/10/chat-girl.html','https://hamroawaz.blogspot.com/2020/06/hajur-aama-ka-katha-amrita-lamsal.html','https://hamroawaz.blogspot.com/2021/04/tamasuk-padam-vishokarma.html','priya sufi']
toprocess={"summer_love":"https://hamroawaz.blogspot.com/2014/02/summer-love-novel.html","saya":"https://hamroawaz.blogspot.com/2014/09/saya-novel.html","aaja_ramita_xa":"https://hamroawaz.blogspot.com/2015/03/aaja-ramita-cha-indra-bahadhur-rai.html","seto_dharti":"https://hamroawaz.blogspot.com/2012/10/seto-dharti.html","karnali_blues":"https://hamroawaz.blogspot.com/2012/11/karnali-blues-buddhisagar-listen-and.html","palpasa_cafe":"https://hamroawaz.blogspot.com/2012/04/palpasa-cafe.html","arki_aimai":"https://hamroawaz.blogspot.com/2020/05/arki-aimai-neelam-karki-niharika.html"}

for (novel, link) in toprocess.items():
    finaldata[novel]=[]

    r=requests.get(link)
    matches=re.findall(r'<source\s+src="[^"]+"',r.text)
    if not matches or len(matches)==1:
        matches=re.findall(r'<a\s+href="[^"]+"',r.text)

    i=0
    for match in matches:
        # match=re.sub(r'\s+', ' ', match)
        l=re.findall('https://archive.org/download/.*?.mp3',match)
        if not l: l= re.findall('https?://.*archive.org.*?.mp3',match)
        if l: l=l[0]
        else: continue
        if '/hawelcome/' in l: continue
        i+=1
        link,length=get_min(l)
        finaldata[novel].append({f"Part {i}":[link, length]})
        if novel in ['summer_love','seto_dharti','arki_aimai']:
            last_item = finaldata[novel][-1]
            last_item['Interview with the author'] = last_item.pop(list(last_item.keys())[0])

    print(novel)
    print('----------------')



# https://play.acast.com/s/poppy-hillstead/fantasyroleplaychatroom
toprocess={"poppy_hillstead":"https://feeds.acast.com/public/shows/ed58a27b-67ad-4075-b336-1b05428be65f","superego":"https://feeds.acast.com/public/shows/63ed978f24c9da0011c1dcbc"}

for (name, url) in toprocess.items():
    finaldata[name]=[]

    page=requests.get(url)
    soup = BeautifulSoup(page.content, "lxml")
    episodes=soup.find_all('item')
    for episode in episodes:
        title=episode.title.text
        if title=='':
            title='EPISODE to_be_replaced'
        print(int(episode.enclosure['length']))
        # if 'EPISODE' not in title: continue
        link=episode.enclosure['url']
        length=int(episode.enclosure['length'])
        duration=((length*4)/128000)//60
        finaldata[name].append({title:[link, int(duration)]})
    finaldata[name].reverse()
    print(name)
    print('----------------')


# http://theblacktapespodcast.com/
finaldata['theblacktapes']=[]

page=requests.get('https://theblacktapes.libsyn.com/rss')
episodes=re.findall('<item>.*?</item>',page.text, re.DOTALL)
for episode in episodes:
    title=re.findall('(?<=<title>).*?(?=</title>)',episode, re.DOTALL)[0]
    if any(ext in title.lower() for ext in ['teaser','bonus','announcement','sleep note','rabbits']): continue
    link=re.findall('https://traffic.libsyn.com/.*?.mp3',episode, re.DOTALL)[0]
    duration=re.findall('(?<=<itunes:duration>).*?(?=</itunes:duration>)',episode, re.DOTALL)[0].split(':')[0]
    finaldata['theblacktapes'].append({title:[link, int(duration)]})
finaldata['theblacktapes'].reverse()
print('theblacktapes')
print('----------------')


# https://castbox.fm/channel/4292481?country=us
finaldata['boju_bajai']=[]
toproceed=["https://everest.castbox.fm/data/episode_list/v2?cid=4292481&eids=395908579%2C395908580%2C395908581%2C395908582%2C395908583%2C395908584%2C395908585%2C395908586%2C395908587%2C395908588%2C395908589%2C395908590%2C395908591%2C395908592%2C395908593%2C395908594%2C395908595%2C395908596%2C395908597%2C395908598&web=1&m=20230920&n=9d54b8da8bcb074b67a1b7e0ec6485aa&r=1","https://everest.castbox.fm/data/episode_list/v2?cid=4292481&eids=395908599%2C395908600%2C395908601%2C395908602%2C395908603%2C395908604%2C395908605%2C395908606%2C395908607%2C395908608%2C395908609%2C395908610%2C395908611%2C395908612%2C395908613%2C395908614%2C395908615%2C395908616%2C395908617%2C395908619&web=1&m=20230920&n=e7d7d1d367df23e2a60785fb2fb8faca&r=1","https://everest.castbox.fm/data/episode_list/v2?cid=4292481&eids=395908620%2C395908621%2C395908622%2C395908623%2C395908624%2C395908625%2C395908626%2C395908627%2C395908628%2C395908629%2C395908630%2C395908631%2C395908632%2C395908633%2C395908634%2C395908635%2C395908636%2C399901060%2C405173578%2C409732935&web=1&m=20230920&n=f79d9a8cdb918d0584ad178ab021d57a&r=1","https://everest.castbox.fm/data/episode_list/v2?cid=4292481&eids=419985751%2C424043589%2C429443629%2C435453738%2C441234060%2C445304175%2C454666222%2C464209745%2C472461236%2C482791889%2C490874493%2C501875686%2C509532518%2C519306524%2C526705725%2C537345095%2C545068292%2C552581979%2C561181176%2C567819381&web=1&m=20230920&n=7a66a4625cf2d41133e91fb29fcc571b&r=1","https://everest.castbox.fm/data/episode_list/v2?cid=4292481&eids=572189519%2C575403634%2C576678534%2C584301801%2C592567723%2C599597534%2C600747198%2C604222248%2C604222249%2C604222250%2C604222251%2C604222252%2C604222253%2C608563997%2C619819526%2C621580732%2C628953796%2C633172056&web=1&m=20230920&n=1e1cc70d35a6e0fd9fdbcd1f5e899ab0&r=1"]

for page in toproceed:
    r=requests.get(page).json()
    for ep in r['data']['episode_list']:
        title=ep['title']
        if not title.startswith('#'):continue
        link=ep['url'].replace('dts.podtrac.com/redirect.mp3/','')
        duration=(int(ep['duration'])/1024)//60
        finaldata['boju_bajai'].append({title:[link,int(duration)]})
print('boju_bajai')
print('----------------')


# https://mirchi.in/podcast/books/saadat-hassan-manto-stories-by-rj-sayema
finaldata['ek_purani_kahani']=[]
r=requests.get('https://api.mymirchi.com/mirchi-feeds/v2/podcast/details?slug=saadat-hassan-manto-stories-by-rj-sayema').json()
episodes=r['response']['seasons'][0]
for ep in episodes:
    title=ep['Title']
    link=re.findall('.*?.mp3',ep['adFreeAudioUrl'])[0]
    duration=int(ep['DurationSeconds']//60)
    finaldata['ek_purani_kahani'].append({title:[link,duration]})
print('ek_purani_kahani')
print('----------------')

# https://archive.org/details/radio-natak-chaubato-episode-66/
temp1='https://archive.org/download/radio-natak-chaubato-episode-66/Radio Natak Chaubato Episode X.mp3'
temp2='https://archive.org/download/radio-natak-chaubato-episode-66/Radio Natak Chaubato episode X.mp3'
temp3='https://archive.org/download/radio-natak-chaubato-episode-66/RadioNatakChaubatoEpisodeX.mp3'
finaldata['chaubato']=[]
for i in range(1,67):
    print(i)
    title=f"Episode {i}"
    if i<31:
        l=temp1.replace('X',str(i).zfill(2))
    elif i<38:
        l=temp2.replace('X',str(i))
    elif i<50:
        l=temp1.replace('X',str(i))
    else:
        l=temp3.replace('X',str(i))
    link,length=get_min(l)
    finaldata['chaubato'].append({title:[link,length]})
print('chaubato')
print('----------------')


# finaldata['atit_ka_panaharu'] = []
# pl = Playlist("https://www.youtube.com/playlist?list=PLzlIsMgCTh1KnuFJJ33G_wBxe1qfrxuQn")
# titlenames = list(range(1, 200))
# if not os.path.exists(outdir+"/Downloads"):
#     os.makedirs(outdir+"/Downloads")
# # cont=False
# for l in pl.video_urls:
#     yt = YouTube(l)
#     music = yt.streams.filter(file_extension="mp4").first()
#     default_filename = music.default_filename
#     # if yt.vid_info['videoDetails']['videoId']=='bNEmyv0GFQ4':cont=True
#     # if not cont: continue
#     if yt.vid_info['videoDetails']['videoId'] in ["vmYentQMXys", "qk7wew1z7Hk", "1XuPf1-3W_k", "xUgdFxMW3Kg",
#                                                   "phnW6L3_RaE", "TZHUQtM-xJQ", "IRQF7IPvJ5Y", "yBA6pIjcy98",
#                                                   "F83_sN6vFmE", "RuiNIYHCybc", "AlZn4f6g9fk", "6mIcxUZ310E",
#                                                   "uaD-U39ABZ4", "wLFTilf50XA", "cu3Rt-h5R8U", "rW69qHHpezw",
#                                                   "Eauq3qyuR34", "zdJHanP_jZo", "nybXSVcGze4"]: continue
#     title = yt.vid_info['videoDetails']['title']
#     match = re.findall(r'\d+', title)
#     if match:
#         if len(match)==1:
#             title = match[0]
#         else: title = match[1]
#     else:
#         title = titlenames[0]
#     if int(title) in titlenames:
#         titlenames.remove(int(title))
#     else:
#         title = titlenames[0]
#         titlenames.pop(0)
#     title=str(int(title))
#     print(title)
#     duration = int(yt.vid_info['videoDetails']['lengthSeconds']) // 60
#
#     finaldata['atit_ka_panaharu'].append({'Episode '+title: ['X', duration]})
#
#     print("Downloading " + default_filename + "...")
#     default_filename=title+'.mp4'
#     while True:
#         try:
#             music.download(output_path=outdir+'/Downloads/',filename=default_filename)
#             break
#         except: pass
#     print("Converting to mp3....")
#     subprocess.call(
#         f"ffmpeg -i {outdir}/Downloads/{default_filename} {outdir}/Downloads/{title}.mp3 -loglevel quiet",
#         shell=True)
#     os.remove(outdir+'/Downloads/'+default_filename)
#     print(finaldata['atit_ka_panaharu'])
#
# def get_episode_number(d):
#     key = next(iter(d))
#     return int(key.split()[-1])
#
# sorted_data = sorted(finaldata['atit_ka_panaharu'], key=get_episode_number)
#
# finaldata.pop('atit_ka_panaharu')
# finaldata['atit_ka_panaharu']=sorted_data

# finaldata['random_shorts']=[{"How visibility is changing the lives of LGBTIQ+ people in Nepal":["https://assets.rumsan.com/clients/recordnepal/How%20visibility%20is%20changing%20the%20lives%20of%20LGBTIQ+%20people%20in%20Nepal.mp3",16]},{"पुष्पा - Real Story by Saigrace":["https://audio.jukehost.co.uk/4Fyg8GrphMSXJ282yWZzEewu9ddqoj9y",11]},{" छिमेकी - Guru Prasad Mainali":["https://audio.jukehost.co.uk/MBQmmdGQpeVQKzsqsS2efSvfhTo56qN9",6]},{"Lets talk about PERIODS. - @RjVashishth":["https://audio.jukehost.co.uk/BJJJ1oIn6JlLON9kcG3rJMEzGyrUSmwo",6]}]

with open(basedir+'/data/play.json', 'w') as xdu:
    json.dump(finaldata, xdu)

for topic in finaldata:
    print(f'app_commands.Choice(name="{topic} ({len(finaldata[topic])})", value="{topic}"),')