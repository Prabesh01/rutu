import os
from shutil import copy, rmtree
import requests
import json
from datetime import datetime, timedelta

# get basedir
filedir=os.path.dirname(os.path.abspath(__file__))
basedir=os.path.dirname(filedir)

from dotenv import load_dotenv
load_dotenv(basedir+'/.env')
ghurl = os.getenv("Github_gist_URL")
fileio_auth_token=os.getenv("fileio_auth_token")
hook=os.getenv("Log_Webhook_URL")

# copy .env file
copy(basedir+'/.env', basedir+'/data/')

# backup crontab file
os.system(f'crontab -l > {basedir}/data/crontab.txt')

# backup mongodb
os.system(f'mongodump -d mim -o {basedir}/data/mongo')
os.system(f'mongodump -d lb -o {basedir}/data/mongo')

# zip data dir
os.system(f'zip -r {basedir}/data.zip {basedir}/data/')

# backup json data
jsoniles=['blob.json','bloboa.json','blobre.json','rnews.json']
for file in jsoniles:
    bin = json.loads(requests.get(ghurl).json()['files'][file]['content'])
    with open(basedir+'/data/'+file, 'w') as xdu:
        json.dump(bin,xdu)

# upload to file.io
r=requests.post('https://file.io/',headers={"Authorization":f"Basic {fileio_auth_token}"},files={'file': ('data.zip', open(basedir+'/data.zip', 'rb'), 'application/zip')},data={'expires': str(datetime.now()+timedelta(days=12)),'maxDownloads': '1','autoDelete': 'true'})

# post link to log webhook
try:
    tosend='<'+r.json()['link']+'>'
except:
    tosend=r.text
data = {'content': tosend}
requests.post(hook, data=data)

# cleanup
toremove=['data.zip','/data/crontab.txt','/data/.env']
for j in jsoniles: toremove.append('/data/'+j)
for to in toremove:
    os.remove(f'{basedir}/{to}')
rmtree(f'{basedir}/data/mongo/')