## Change Log

### 21 Sept 2023

- added scripts/play.py and data/play.json
- in cogs/voice.py, changed hybrid play command to text command and added /play slash command. also added duration indicator in swosthani
- removed 19 command in cogs/on_message
- modified r!help data in vars.py to remove 19 and add /play
- changed cogs/news.py footerr
- in cogs/on_message
  - increased mim timeout from 15 to 30 secs
  - modified mim autoremove on 10 disikes to 60% rule
  - fixed dm msg attachment_url formatting
  - changed self.dmerz to vars.dmerz
- removed 'rond' keyword reply.
- in server.bot.py
  - fixed setopati and onlikekhabar function's lastone and tme bug
  - memcnt[gid]>=2 from 4
  - fixed setopati incorrect image bug
- in cogs/general.py, changed r!inv link and added /status app command
- in cogs/tasks.py, fixed t_response and added bot status change feature.
- added try catch in msg send and webhook functions and tasks
- added charater limit to ai promt
- changed http.server port in sendameme function and readme. also added index.html to exception file in mim dir