import os, json
from discord.ext import commands
import nepali_datetime
from datetime import datetime
from datetime import date
from datetime import timedelta
import pytz
import discord
import vars
import requests
from random import randrange, choice, choices
import asyncio
from bs4 import BeautifulSoup
from discord_webhook import DiscordWebhook, DiscordEmbed
from nepali_unicode_converter.convert import Converter
from PIL import Image, ImageDraw, ImageFont
import uuid
from cogs.checks import guard_uncle
from collections import defaultdict

from wrapt_timeout_decorator import timeout
from asgiref.sync import sync_to_async

import os
import openai
openai.api_key = os.getenv("OpenAI_API_KEY")

#import google.generativeai as genai
#genai.configure(api_key=os.getenv("GEMINI_API_KEY"))
#model = genai.GenerativeModel(model_name='gemini-pro')

converter = Converter()

tz_NP = pytz.timezone('Asia/Kathmandu')

xutti = json.load(open(vars.basepath+'/out/xutti.json'))

# for the command: 123
lisn = ['क', 'ख', 'ग', 'घ', 'ङ', 'च', 'छ', 'ज', 'झ', 'ञ', 'ट', 'ठ', 'ड', 'ढ', 'ण', 'त', 'थ', 'द', 'ध', 'न', 'प', 'फ',
        'ब', 'भ', 'म', 'य', 'र', 'ल', 'व', 'श', 'ष', 'स', 'ह', 'क्ष', 'त्र', 'ज्ञ']

# for the command: कखरा
numcnt = ["एक", "दुई", "तीन", "चार", "पाँच", "छ", "सात", "आठ", "नौ", "दस", "एघार", "बाह्र", "तेह्र", "चौध", "पन्ध्र",
          "सोह्र", "सत्र", "अठार", "उन्नाइस", "बीस", "एक्काइस", "बाइस", "तेइस  ", "चौबीस  ", "पच्चीस  ", "छब्बीस  ",
          "सत्ताइस  ", "अट्ठाइस  ", "उनान्तीस  ", "तीस  ", "एकतिस ", "बत्तीस  ", "तेत्तीस  ", "चौंतीस  ", "पैंतीस  ",
          "छत्तीस  ", "सैंतीस  ", "अड्तीस  ", "उनान्चालिस  ", "चालीस  ", "एकचालीस  ", "बयालीस  ", "त्रिचालिस  ",
          "चौवालिस  ", "पैंतालिस  ", "छियालिस  ", "सड्चालिस  ", "अड्चालीस  ", "उनान्पचास  ", "पचास  ", "एकाउन्न  ",
          "बाउन्न  ", "त्रिपन्न  ", "चौवन्न  ", "पचपन्न  ", "छप्पन्न  ", "सन्ताउन्न  ", "अन्ठाउन्न  ", "उनान्साठी  ",
          "साठी  ", "एकसठ्ठी  ", "बैसठ्ठी  ", "त्रिसठ्ठी  ", "चौंसठ्ठी  ", "पैंसठ्ठी  ", "छैसठ्ठी  ", "सड्सठ्ठी  ",
          "अड्सठ्ठी  ", "उनान्सत्तरी  ", "सत्तरी  ", "एकहत्तर  ", "बहत्तर  ", "त्रिहत्तर  ", "चौहत्तर  ", "पचहत्तर  ",
          "छिहत्तर  ", "सतहत्तर  ", "अठहत्तर  ", "उनासी  ", "अस्सी  ", "एकासी  ", "बयासी  ", "त्रियासी  ", "चौरासी  ",
          "पचासी  ", "छियासी  ", "सतासी  ", "अठासी  ", "उनानब्बे  ", "नब्बे  ", "एकानब्बे  ", "बयानब्बे  ",
          "त्रियानब्बे  ", "चौरानब्बे  ", "पन्चानब्बे  ", "छियानब्बे  ", "सन्तानब्बे  ", "अन्ठानब्बे  ", "उनान्सय  ",
          "सय  "]

image_types = ["png", "jpeg", "gif", "jpg", "mp4"]

deusilist=[
    "@here\n### ♪♪ हरियो  गोबर  ले  लिपेको \n### लक्ष्मी  पूजा  गरेको \n### हे ! औंसी  को  बारो \n### गाई  तिहारो  भैलो  ♪♪", # @everyone\n
    ["https://i.ibb.co/ZLYZBpw/1.jpg","https://i.ibb.co/Ny1RNMD/2.jpg","https://i.ibb.co/JjWbVTY/3.png","https://i.ibb.co/YZF7S3k/4.png"],
    "### आहै ! भन मेरा भाइ हो",
    "### आहै ! राम्ररी भन",
    "### आहै ! स्वर मिलाईकन",
    "### आहै ! देउसीरे भन",
    "### Aahai Jhilimili Jhilimili",
    "### Aahai K Ko Jhilimili",
    "### Aahai Phula Ko Jhilimili",
    "### Ahai Batti Ko Jhilimili",
    "### ए हामी त्यसै",
    "### ए आएका हैनौँ",
    "### ए बलि  राजा  को",
    "### ए  हुकुमै ले",
    "### ए रातो पिङ", # माटो
    "### ए  सोलो नेट", # चिप्लो बाटो
    "### ए लड्दै र पड्दै",
    "### ए देउसीरे खेल्न",
    "### ए आएका हामी",
    "### आहै ! यो  सर्भर कत्रो",
    "### आहै ! जय नेपाल जत्रो",
    "### ए आखुम पाखुम",
    "### ए सेलरोटी चाखुम",
    "### ए छानामाथी घिरौला",
    "### ए के के दिन्छन् हेरौला",
    "### ए छानामाथी अधुवा",
    "### ए सर्भर mod पधुवा",
    "### आहै ! भट्टयाउने को",
    "### आहै ! स्वरै सुक्यो",
    "### आहै ! साइकल को घण्टी",
    "### आहै ! छिटो गर्नुस् अन्टी",
    "### ए दिन्छन दिन्छन",
    "### ए nitro दिन्छन",
    "### ए owo cash",
    "### ए dank memer blc",
    "### ए दिनै आँटे",
    "### आहै ! भन मेरा भाइ हो ",
    "### आहै ! देउसीरे भन",
    "### आहै ! घोडा बान्ने तबेला",
    "### आहै ! हामीलाई भयो अबेला",
    "### आहै ! चरि चुच्चो",
    "### आहै ! के सारो छुच्चो",
    "### ♪♪ धान को बाला कोदा को कपनि\n### हामी गयौ बस हौ लक्ष्मी  ♪♪",
    "https://media.tenor.com/Kf34YfgoNkgAAAAC/happytihar-happy-diwali.gif"
    ]
deusilen=len(deusilist)

async def mcheck(a,b):
    return not await guard_uncle(None, None, a, b)


async def eventbuilder(nepdate,engdate):
    nefoli=[]
    availableevz=os.listdir(vars.basepath+'/data/events/')
    tocheck=[engdate,engdate.split(',')[0],nepdate.split(',')[0][:-5],nepdate.split(',')[0]]
    matches = [element for element in availableevz if element in tocheck]
    for match in matches:
        fp = open(vars.basepath+'/data/events/' + match, "r")
        nefoli.append(':stadium: ' + fp.read()[:-1])
        fp.close()
    if nefoli:
        return '\n'+'\n'.join(nefoli)
    else:
        return ''


async def saveimg(attch, attchurl):
    ext=attchurl.split('.')[-1].split('?')[0]
    if ext not in ['jpg','jpeg','png','gif','webp']: return False
    with open(attch, 'wb') as at:
        at.write(requests.get(attchurl).content)
    return True


async def make_square(attch, min_size=256, fill_color=(0, 0, 0, 0)):
    im = Image.open(attch)
    x, y = im.size
    size = max(min_size, x, y)
    new_im = Image.new('RGBA', (size, size), fill_color)
    new_im.paste(im, (int((size - x) / 2), int((size - y) / 2)))
    new_im.save(attch, "PNG")


async def sendameme(message,u_collection,r_collection, dupav=None,mimsent=None):
    mimpath=vars.basepath + '/data/mim'
    file_list = os.listdir(mimpath)
    file_list.sort(key=lambda x: os.path.getctime(os.path.join(mimpath, x)), reverse=True)
    weights = [1.0 / (i + 1) for i in range(len(file_list))]
    while True:
        file = choices(file_list, weights=weights, k=1)[0]
        if mimsent:
            if file in dupav: continue
            if '.mp4' in file or '.gif' in file: continue
        if file not in ['.gitignore','index.html'] : break
    filetosend = vars.basepath + '/data/mim/' + file
    if r_collection.count_documents({'mimid': str(file)}) > 0:
        lik = r_collection.count_documents({"$and": [{"mimid": str(file)}, {"react": 1}]})
        dlik = r_collection.count_documents({"$and": [{"mimid": str(file)}, {"react": -1}]})
    else:
        lik = 0
        dlik = 0
    if dlik > 9:
        if lik<round(0.6*dlik):
            if mimsent: vars.votex.pop(mimsent.id)
            os.remove(filetosend)
            u_collection.delete_many({"mimid": str(file)})
            r_collection.delete_many({"mimid": str(file)})
            return None, None
    footed = '👍 ' + str(lik) + ' | 👎 ' + str(dlik)
    if os.path.exists(vars.basepath + '/out/mimcap'):
        with open(vars.basepath + '/out/mimcap', 'r') as ft:
            deszf = ft.read()
    else: deszf = choice(vars.desz)
    embed = discord.Embed(title="Nepali meme", description=deszf)
    embed.set_footer(text=footed, icon_url="")
    if '.mp4' in str(file):
        try:
            mimsent = await message.channel.send('> ' + footed, file=discord.File(filetosend))
        except:
            return
        try:
            await mimsent.add_reaction('👍')
            await mimsent.add_reaction('👎')
        except:
            pass
    else:
        try:
            embed.set_image(url="http://91.208.197.189:8000/mim/" + file)
            if mimsent:
                await mimsent.edit(embed=embed)
            else:
                mimsent = await message.channel.send(embed=embed)
            try:
                await mimsent.add_reaction('👍')
                await mimsent.add_reaction('👎')
                await mimsent.add_reaction('▶️')
            except:
                pass
        except:
            pass

    vars.votex[mimsent.id] = file
    return file, mimsent


@timeout(35)
async def openaiAnswer(message, question, attachment=None):
    if attachment:
        response = openai.Image.create_variation(image=open(attachment, 'rb'), n=1, size='512x512')
        with open(attachment + '.jpg', 'wb') as at:
            at.write(requests.get(json.loads(json.dumps(response['data']))[0]['url'].split('\n')[0]).content)
        try: await message.reply(file=discord.File(attachment + '.jpg'))
        except: pass
        os.remove(attachment)
        os.remove(attachment + ".jpg")
        return
    vip = [843758119065747458, 736529187724197951]
    authid=message.author.id
    if any('<@' + str(ele) + '>' in question for ele in vip) and authid != 736529187724197951:
        for i in message.mentions:
            if i.id in vip:
                vipp = i.name
                if authid != i.id:
                    break
        snd = await message.channel.send(
                f"You don't meet standards to ping {vipp}, loser! What an idiot. Fking moron.")
        vars.jusnow.append(snd.id)
        return
    if authid in vars.gptlimit:
        await message.reply('slow down buddy. 2 queries per min')
        return
    elif authid in vars.spammer:
        vars.gptlimit.append(authid)
        vars.spammer.remove(authid)
    else:vars.spammer.append(authid)

    async with message.channel.typing():
        questionz = []
        if (
                'you' or 'You' or ' u' or ' ur ' or ' thy ' or 'own' or 'made' or 'found' or 'develop' or 'build' or 'built' or 'Prabesh' or 'prabesh' or '736529187724197951' or 'Routiney' or 'routiney') in question:
            questionz.append({"role": "system", "content": vars.assume})
        if message.channel.id in [788423186546819083, 839667499267522631]:
            questionz.append({"role": "system", "content": vars.mongo_tom})
        questionz.append({"role": "system", "content": vars.clydepmt})
        questionz.append({"role": "user", "content": question})

        try:
            response = await sync_to_async(openai.ChatCompletion.create)(
                model="gpt-3.5-turbo",
                messages=questionz,
                temperature=0.9,
                max_tokens=1063,
                top_p=1,
                frequency_penalty=0,
                presence_penalty=0.6,
                stop=[" Human:", " AI:", ]
            )
        except Exception as e:
            await message.channel.send(str(e))
            return
        text = json.loads(json.dumps(response['choices']))[0]['message']['content']
        n = 1990
        chunks = [text[i:i + n] for i in range(0, len(text), n)]
        try:
            chkcnt = 0
            for chk in chunks:
                if chkcnt == 0:
                    chksplt=chk.split('\n')
                    chkstrp=chk.strip()
                    if len(chksplt) > 1:
                        if len(chksplt[0]) <= 10:
                            chk = chk.replace(chksplt[0], '', 1)
                    if chkstrp[0] == '"' and chkstrp[-1:] == '"':
                        chk = chkstrp[1:-1]
                    chkcnt = 1
                chk = chk.replace('@everyone', '@_everyone')
                chk = chk.replace('@here', '@_here')
                snd = await message.channel.send(chk)
                vars.jusnow.append(snd.id)
        except:
            return


async def geminiAnswer(message, question):
    vip = [843758119065747458, 736529187724197951]
    authid=message.author.id
    if any('<@' + str(ele) + '>' in question for ele in vip) and authid != 736529187724197951:
        for i in message.mentions:
            if i.id in vip:
                vipp = i.name
                if authid != i.id:
                    break
        snd = await message.channel.send(
                f"You don't meet standards to ping {vipp}, loser! What an idiot. Fking moron.")
        vars.jusnow.append(snd.id)
        return
    if authid in vars.gptlimit:
        await message.reply('slow down buddy. 2 queries per min')
        return
    elif authid in vars.spammer:
        vars.gptlimit.append(authid)
        vars.spammer.remove(authid)
    else:vars.spammer.append(authid)

    async with message.channel.typing():
        questionz = []
        if ('you' or 'You' or ' u' or ' ur ' or ' thy ' or 'own' or 'made' or 'found' or 'develop' or 'build' or 'built' or 'Prabesh' or 'prabesh' or '736529187724197951' or 'Routiney' or 'routiney') in question:
            questionz.append({"role": "user", "parts": [vars.assume]})
            questionz.append({"role": "model", "parts": ["Hello, its me Routiney!"]})
        questionz.append({"role": "user", "parts": [vars.clydepmt]})
        questionz.append({"role": "model", "parts": ["Okay"]})
        questionz.append({"role": "user", "parts": [question]})

        try:
            response = await sync_to_async(model.generate_content)(questionz)
        except Exception as e:
            await message.channel.send(str(e))
            return
        try:
            text = response.text
        except:
            print(response.parts)
            text="dont wanna bother replying to that"
			
        n = 1990
        chunks = [text[i:i + n] for i in range(0, len(text), n)]
        try:
            for chk in chunks:
                chk = chk.replace('@everyone', '@_everyone')
                chk = chk.replace('@here', '@_here')
                snd = await message.channel.send(chk)
                vars.jusnow.append(snd.id)
        except:
            return



class MessagesCog(commands.Cog):

    def __init__(self, bot):
        self.bot = bot
        self.ingame=[]
        self.blocklistx=[]
        self.acount=0
        self.servlist= defaultdict(lambda: {'score': 0, 'serverName': ''})
        self.lastuser=0
        self.duplimit=0
        self.u_collection = self.bot.db.u_collection
        self.r_collection = self.bot.db.r_collection
        self.s_collection = self.bot.db.s_collection

    def update_server_data(self,server_id, server_name):
        self.servlist[server_id]['score'] += 1
        self.servlist[server_id]['serverName'] = server_name

    @commands.Cog.listener()
    async def on_ready(self):
        self.blocklistx = json.load(open(vars.basepath + '/data/mimban.json'))['u']
        self.duplimit=(round(len(os.listdir(vars.basepath + '/data/mim'))/2))-1

    @commands.Cog.listener()
    async def on_message(self, message):

        # auto-publish messages on annoucement channels. not a public feature but usefull one
        if message.guild and message.channel.type == discord.ChannelType.news:
            try:
                await message.publish()
            except:
                pass

        # add Repost emoji to news so that users can share it easily.
        if str(message.author) == 'Routiney#0000':
            try:
                await message.add_reaction('🔁')
            except:
                pass
            return

        if message.author == self.bot.user: return
        if message.author.bot: return

        msg_= message.content.lower()

        if msg_ == 'cal':
            if await mcheck('cal',message): return
            await message.channel.send(file=discord.File(vars.basepath+"/out/cal.png"))
            return

        elif msg_=="bida":
            if await mcheck('bida', message): return
            okii = []
            text = '-{d.month}-'.format(d=nepali_datetime.datetime.now())
            textday = '-{d.month}-{d.day}'.format(d=nepali_datetime.datetime.now())
            todayday = '{d.day}'.format(d=nepali_datetime.datetime.now())
            todaybida = 0
            nextbida = 0
            gotone = 0
            for txt in xutti.keys():
                if txt.endswith(textday):
                    todaybida = 1
                if text in txt:
                    try:
                        okii.append('```apache\n♦️ ' + xutti[txt] + '\n```')
                        if gotone == 0 and int(txt[-3:].split('-')[1]) > int(todayday):
                            nextbida = int(txt[-3:].split('-')[1]) - int(todayday)
                            gotone = 1
                    except:
                        pass
            bidadesc = str(len(okii)) + " bida(s) this month!"
            if todaybida == 1:
                bidadesc = "Aaja xutti!!"
            if nextbida != 0:
                bidadesc = bidadesc + "\nNext holiday in " + str(nextbida) + " days."
            else:
                if todaybida == 0:
                    bidadesc = bidadesc + "\nbut no upcomming holidays :("
                else:
                    bidadesc = bidadesc + "\nbut no upcomming holidays this month."
            if len(okii) == 0:
                bidadesc = "TF! No holiday this month :/\nHope this is just some error in the bot"

            await message.channel.send(f"**{bidadesc}**\n{' '.join(okii)}")
            return

        elif msg_=="t" or msg_=="today":
            if await mcheck('t', message): return
            try:
                now = datetime.now(tz_NP)
                todaynefol=vars.todaynefol
                today_info=todaynefol.split('\n')
                np_date=today_info[0].split(',')[0].replace(':flag_np:', '').strip()
                eng_date=today_info[2]
                if not ':date:' in eng_date: eng_date=today_info[3]
                eng_date=eng_date.replace(':date:', '').strip()

                await message.channel.send(todaynefol +await eventbuilder(np_date,eng_date)+ '\n:clock3: ' + now.strftime("%I:%M.%S %p"))
            except:
                pass
            return

        elif msg_.startswith('t+') or msg_.startswith('t-'):
            if await mcheck('t+-', message): return
            if len(msg_.split(' ')) != 1: return
            if '-' in msg_: divd='-'
            else: divd='+'
            giben = message.content.split(divd)[1]
            if not giben.isnumeric():return
            try:
                if divd=='+':
                    eng_date = datetime.now(tz_NP) + timedelta(days=int(giben))
                    np_date = nepali_datetime.datetime.now() + timedelta(days=int(giben))
                else:
                    eng_date = datetime.now(tz_NP) - timedelta(days=int(giben))
                    np_date = nepali_datetime.datetime.now() - timedelta(days=int(giben))
            except:
                await message.channel.send(
                    'https://indianmemetemplates.com/wp-content/uploads/aae-band-kar-band-kar.jpg')
                return
            evzt=await eventbuilder(np_date.strftime("%d %B %Y, %G"), eng_date.strftime("%b %d, %Y"))
            date_info = await vars.hp_date_info(np_date, eng_date)
            await message.channel.send(f"{date_info}{evzt}")
            return

        elif msg_ == 'कखरा':
            if message.author.id in self.ingame: return
            if await mcheck('कखरा', message): return
            self.ingame.append(message.author.id)
            while True:
                ok = randrange(35)
                inde = lisn.index(str(lisn[ok]))
                try:
                    await message.reply(lisn[ok])
                except:
                    break

                def check(m):
                    return m.author == message.author and m.channel == message.channel

                try:
                    msg = await self.bot.wait_for('message', check=check, timeout=15.0)
                except asyncio.TimeoutError:
                    try:
                        await message.reply('Timeout! Correct answer is ' + lisn[inde + 1])
                        break
                    except:
                        break
                if msg.content == '।':
                    try:
                        await msg.add_reaction('👌')
                    except:
                        pass
                    break
                if msg.content == lisn[inde + 1]:
                    try:
                        await msg.add_reaction('✅')
                    except:
                        break
                else:
                    try:
                        await msg.add_reaction('❌')
                        await msg.reply('Correct answer is ' + lisn[inde + 1])
                    except:
                        break
            self.ingame.remove(message.author.id)
            return

        elif msg_ == '123':
            if message.author.id in self.ingame: return
            if await mcheck('123', message): return
            self.ingame.append(message.author.id)
            while True:
                kok = randrange(99)
                ans = str(kok + 1)
                try:
                    await message.reply(numcnt[kok])
                except:
                    break

                def check(m):
                    return m.author == message.author and m.channel == message.channel

                try:
                    msg = await self.bot.wait_for('message', check=check, timeout=15.0)
                except asyncio.TimeoutError:
                    try:
                        await message.reply('Timeout! Correct answer is ' + ans)
                        break
                    except:
                        break
                if msg.content == '0':
                    try:
                        await msg.add_reaction('👌')
                    except:
                        pass
                    break
                if msg.content == ans:
                    try:
                        await msg.add_reaction('✅')
                    except:
                        break
                else:
                    try:
                        await msg.add_reaction('❌')
                        await msg.reply('Correct answer is ' + ans)
                    except:
                        break
            self.ingame.remove(message.author.id)
            return

        elif msg_.startswith('rf'):
            if len(msg_.split(' ')) == 1:
                if await mcheck('rf', message): return
                num=msg_[2:]
                if msg_=='rf':
                    try:
                        embed = discord.Embed(title="Rashifal (rf)", description="Command Example: 'rf1' for Mesh",
                                              color=0x9266cc)
                        embed.add_field(name="​", value="1  - ♈ मेष (Aries)", inline=True)
                        embed.add_field(name="​", value="2  - ♉ वृष (Taurus)", inline=True)
                        embed.add_field(name="​", value="3  - ♊ मिथुन (Gemini)", inline=True)
                        embed.add_field(name="​", value="4  - ♋ कर्कट (Cancer)", inline=True)
                        embed.add_field(name="​", value="5  - ♌ सिंह (Leo)", inline=True)
                        embed.add_field(name="​", value="6  - ♍ कन्या (Virgo)", inline=True)
                        embed.add_field(name="​", value="7  - ♎ तुला (Libra)", inline=True)
                        embed.add_field(name="​", value="8  - ♏ वृश्चिक (Scorpius)", inline=True)
                        embed.add_field(name="​", value="9  - ♐ धनु (Sagittarius)", inline=True)
                        embed.add_field(name="​", value="10 - ♑ मकर (Capricornus)", inline=True)
                        embed.add_field(name="​", value="11 - ♒ कुम्भ (Aquarius)", inline=True)
                        embed.add_field(name="​", value="12 - ♓ मीन (Pisces)", inline=True)
                        await message.channel.send(embed=embed)
                    except:
                        pass
                    return
                elif num.isnumeric():
                    if 1>int(num)>12: return
                    try:
                        myne=vars.horos[num]
                        rfurl = f"https://www.hamropatro.com/rashifal/daily/{myne[0]}"
                        page = requests.get(rfurl)
                        soup = BeautifulSoup(page.content, "html.parser")
                        rashi = soup.find("div", class_="desc").find("p").text.strip()
                        if num=='12':
                            rashi='-'.join(rashi.split('-')[:-1])
                        embed = discord.Embed(title=myne[1], description=rashi,
                                              color=choice(myne[2]))
                        embed.set_author(name="Hamro Patro", url=f"https://www.hamropatro.com/rashifal/daily/{myne[0]}",
                                         icon_url="https://www.hamropatro.com/images/hamropatro.png")
                        embed.set_thumbnail(url=f"https://www.hamropatro.com/images/dummy/ic_sodiac_{num}.png")
                        await message.channel.send(embed=embed)
                    except:
                        pass
                    return

        if msg_.startswith('ba ') or msg_.startswith('ab '):
            if len(msg_.split('-')) == 3 and len(msg_.split(' ')) == 2:
                if await mcheck('ab/ba', message): return
                gibenz = msg_.split(' ')[1]
                gibenzi=gibenz.split('-')
                if (gibenzi[0].isnumeric() and gibenzi[1].isnumeric() and gibenzi[2].isnumeric()):
                    try:
                        if msg_.startswith('ba '):
                            tosend=nepali_datetime.date(int(gibenzi[0]), int(gibenzi[1]),
                                                 int(gibenzi[2])).to_datetime_date()
                        else:
                            tosend=nepali_datetime.date.from_datetime_date(
                            date(int(gibenzi[0]), int(gibenzi[1]), int(gibenzi[2])))
                        await message.channel.send(
                            tosend
                            )
                        return
                    except:
                        await message.channel.send('invalid argument')
                        return
                else:
                    await message.channel.send('invalid argument')
                    return

        if msg_ == "mim" and message.attachments:
                uid=message.author.id
                if uid in self.blocklistx:
                    await message.reply(
                            'You are banned from meme uploading platform either because you tried to spam or you uploaded non-meme contents!')
                    return
                if await mcheck('mim-up', message): return
                attachnt = 0
                for attachment in message.attachments:
                    attachnt += 1
                    ext=attachment.filename.split('.')[-1].lower()
                    if ext not in image_types:
                        await message.channel.send(
                            "Attachment-" + str(attachnt) + ": Only png, jpg, jpeg, gif and mp4 files allowed!")
                        continue
                    while True:
                        savzz = str(uuid.uuid4())+'.'+ str(ext)
                        tosavepath = vars.basepath + '/data/mim/' + savzz
                        if not os.path.exists(tosavepath): break
                    #     result = u_collection.find_one({'mimid': savzz})
                    #     if result is None: break
                    # savzz = savzz +'.'+ str(ext)
                    await attachment.save(tosavepath)
                    if os.path.getsize(tosavepath) > 5000000:
                            os.remove(tosavepath)
                            await message.channel.send(
                                'Attachment-' + str(attachnt) + ': Max upload size limit is 5MB!')
                            continue
                    self.u_collection.insert_one({"uid":uid,"mimid":savzz,"date":datetime.now()})
                await message.add_reaction('✅')
                return

        if message.guild is None:
            channel = self.bot.get_channel(888624067656187924)
            attachment_url = []
            try:
                attachments = message.attachments
                for attachment in attachments:
                    attachment_url.append(attachment.url)
            except:
                pass
            if not attachment_url:
                sentmsg=await channel.send(f"[{message.author}](http://91.208.197.189/data/u/?id={message.author.id}) sent:\n```{message.content}```")
            else:
                sentmsg=await channel.send(f"[{message.author}](http://91.208.197.189/data/u/?id={message.author.id}) sent:\n{attachment_url}\n```{message.content}```")

            if message.channel.id in vars.dmerz:
                return
                # response = random.choice(oar)
                # await message.channel.send(response)
            else:
                await message.channel.send(
                    "Use r!sub to setup bot and r!help to see command list.\n\nFor further support regarding the bot, contact _prabesh or join the support server:\nhttps://discord.com/invite/ehRSPHuTDy")
                vars.dmerz.append(message.channel.id)
                try: await sentmsg.add_reaction('🔗')
                except: pass
            return

        if message.channel.id == 888624067656187924:
            if message.reference:
                tosend = await message.channel.fetch_message(message.reference.message_id)
                if 'sent:' in tosend.content and not '-->' in tosend.content:
                    uid = tosend.content.split("?id=")[1].split(")")[0]
                    if len(uid) < 10:return
                    if uid.isnumeric(): uid = int(uid)
                    else: return
                    msg = message.content
                else:
                    # await message.delete()
                    return
            else:
                if len(message.content.split(':')[0]) >= 10:
                    try:uid = int(message.content.split(':')[0])
                    except:return
                    msg = ' '.join(message.content.split(':')[1:])
                else:
                    # await message.delete()
                    return
            try:
                if uid == 736529187724197951:
                    await message.add_reaction('❌')
                    return
                user = await self.bot.fetch_user(uid)
                await user.send(msg)
                await message.add_reaction('👌')
            except:
                return
            return

        if msg_.startswith('https://discord.com/') and len(msg_.split()) == 1:
            lnksplit=msg_.split('/')
            if len(lnksplit)!=7: return
            if await mcheck('msg_url_quote', message): return
            footer = ''
            guild = message.guild

            member = guild.me
            perm_list = [perm[0] for perm in message.channel.permissions_for(member) if perm[1]]
            if 'manage_webhooks' not in perm_list or 'manage_messages' not in perm_list:
                return

            if str(message.guild.id) != lnksplit[4]:
                try:
                    guild = self.bot.get_guild(int(lnksplit[4]))
                    footer = guild.name + ' · '
                except:
                    return
            try:
                channlz = discord.utils.get(guild.channels, id=int(lnksplit[5]))
                ques = await channlz.fetch_message(int(lnksplit[6]))
            except:
                return
            if channlz == None or ques == None:
                return
            footer += channlz.name

            uniurl = ''
            webhooks = await message.channel.webhooks()
            for webhook in webhooks:
                if webhook.user == 'Routiney#9013':
                    uniurl = webhook.url
                    break
            if uniurl == '':
                try: uniurl = await message.channel.create_webhook(name='uni')
                except: return
                uniurl = uniurl.url
            try:await message.delete()
            except: pass
            try:
                attachment_url = ques.attachments[0].url
            except:
                attachment_url = ''
            webhuni = DiscordWebhook(username=str(message.author.name), avatar_url=str(message.author.avatar.url),
                                     url=uniurl)
            embe = DiscordEmbed(title="Go to message",
                                url="https://discord.com/channels/" + str(message.guild.id) + "/" + str(
                                    channlz.id) + "/" + str(ques.id), description=ques.content)
            embe.set_author(name=str(ques.author.name),
                            url="https://discord.com/channels/" + str(message.guild.id) + "/" + str(
                                channlz.id) + "/" + str(
                                ques.id), icon_url=str(ques.author.avatar.url))
            embe.set_footer(text=f'#{footer}')
            try:
                embeb = ques.embeds[0].to_dict()
                webhuni.add_embed(embe)
                webhuni.add_embed(embeb)
            except:
                embe.set_image(url=attachment_url)
                webhuni.add_embed(embe)

            webhuni.execute()

        elif msg_ == "r!":
            if await mcheck('r!', message): return
            member = message.guild.me
            perm_list = [perm[0] for perm in message.channel.permissions_for(member) if perm[1]]
            if 'send_messages' not in perm_list:
                try:
                    await message.author.send(f"lmao. I don't have permission to send message in <#{message.channel.id}>")
                except:
                    self.bot.AppInfo = await self.bot.application_info()
                    await self.bot.AppInfo.owner.send(str(message.author) + ' from ' + str(message.guild) + ' (' + str(
                        message.guild.id) + ') is dumb af - r!')
                return
            for perm in vars.required_perms:
                if perm not in perm_list:
                    await message.channel.send(
                        f"Grrrr. I don't have permission to {perm.replace('_',' ')}. {vars.required_perms[perm]}")
                    return
            if 'administrator' in perm_list:
                await message.channel.send(
                    "Everything is just perfect!\nFeel free to contact me if you face any issue: _prabesh")
                return
            await message.channel.send(
                "Looks like you set me up well\nFeel free to contact me if you face any issue: _prabesh")
            return

        elif msg_ == 'del' and message.author.id == 736529187724197951:
            try:
                messaged = await message.channel.fetch_message(message.reference.message_id)
                await messaged.delete()
                await message.delete()
            except:
                pass
            return

        elif (msg_ == 'rem' or msg_=='ban' or msg_=='who'):
            if msg_=="ban" and message.author.id != 736529187724197951: return
            mrefr=message.reference
            if not mrefr: return
            mimmsg = await message.channel.fetch_message(mrefr.message_id)
            if mimmsg.author.id!=786534057437691914:return

            mimemb = mimmsg.embeds
            mimfil=mimmsg.attachments
            if mimemb:
                mimimg = mimemb[0].image.url
            elif mimfil:
                mimimg = mimmsg.attachments[0].url
            else: return

            if mimimg is None: return
            mimid=mimimg.split('/')[-1].split('?')[0]
            emo='👌'
            
            if msg_=='rem' and message.author.id == 736529187724197951:
                os.remove(vars.basepath +'/data/mim/'+mimid)
                self.u_collection.delete_many({"mimid": mimid})
                self.r_collection.delete_many({"mimid": mimid})
                await message.add_reaction(emo)
                return
                
            result = self.u_collection.find_one({"mimid": mimid})
            if not result: return
            if msg_=='rem':
                if message.author.id != result["uid"]: 
                    await message.add_reaction('❌')
                    return
                os.remove(vars.basepath +'/data/mim/'+mimid)
                self.u_collection.delete_many({"mimid": mimid})
                self.r_collection.delete_many({"mimid": mimid})
            elif msg_=='who':
                await message.reply(f'<@{result["uid"]}>')
                return
            else:    
                if not result['uid'] in self.blocklistx:
                    self.blocklistx.append(result['uid'])
                    emo='🔒'
                else: 
                    self.blocklistx.remove(result['uid'])
                    emo='🔓'
                with open(vars.basepath + '/data/mimban.json', 'w') as xdu:
                    json.dump({'u':self.blocklistx}, xdu)
            await message.add_reaction(emo)
            return

        elif msg_.startswith("uni "):
            if await mcheck('uni', message): return
            member = message.guild.me
            perm_list = [perm[0] for perm in message.channel.permissions_for(member) if perm[1]]
            try:
                webhooks = await message.channel.webhooks()
                uniurl = ''
                for webhook in webhooks:
                    if webhook.user.id == self.bot.user.id:
                        uniurl = webhook.url
                        break
                if uniurl == '':
                    try: uniurl = await message.channel.create_webhook(name='uni')
                    except: return
                    uniurl = uniurl.url
                await message.delete()
                webhuni = DiscordWebhook(username=str(message.author.name), avatar_url=str(message.author.avatar.url),
                                         url=uniurl, content=converter.convert(message.content.split('uni ')[1]))
                webhuni.execute()
                return
            except:return
            return

        elif msg_ == 'fms':
            if await mcheck('fms', message): return
            try:
                vc = discord.utils.get(self.bot.voice_clients, guild=message.channel.guild)
                await vc.disconnect()
            except:pass
            try:
                await message.add_reaction('👌')
            except:
                pass
            return

        elif msg_.startswith('pls '):
            subcmd=msg_.split(' ')[1]
            if subcmd not in ["nepal", "nepalify", "vary", "love","oli","boka","babe","milf"]: return
            if message.author.id in vars.spammer:
                await message.add_reaction('🚫')
                await message.reply("You are being rate limited! Try again after a minute.", delete_after=5)
                return
            if await mcheck('pls '+subcmd, message): return
            if message.author.id != 736529187724197951:
                vars.spammer.append(message.author.id)
            if subcmd in ["babe","milf"]:
                if str(message.guild.id) in vars.damnoa:
                    await message.channel.send('NSFW commands are disabled in this server', delete_after=5)
                    return

            attch = vars.basepath + '/out/' + str(message.id)
            attchurl=''
            if message.reference:
                ques = await message.channel.fetch_message(message.reference.message_id)
                if ques.attachments:
                    attchurl = ques.attachments[0].url
            if not attchurl:
                if message.attachments:
                    attchurl = message.attachments[0].url
                elif message.mentions.__len__() > 0:
                    attchurl = message.mentions[0].avatar.url
                else:
                    attchurl = message.author.avatar.url
            if not await saveimg(attch, attchurl): return
            await message.reply('Ah shit. This might take a while', delete_after=2)

            if subcmd=="nepal" or subcmd == "nepalify":
                img1 = Image.open(attch).convert('RGBA').resize((480, 480))
                img2 = Image.open(vars.basepath+'/assets/frame.gif')
                img1.putalpha(128)
                out = []
                for i in range(0, img2.n_frames):
                    img2.seek(i)
                    f = img2.copy().convert('RGBA').resize((480, 480))
                    f.paste(img1, (0, 0), img1)
                    out.append(f.resize((256, 256)))
                out[0].save(attch + ".gif", save_all=True, append_images=out[1:], loop=0, disposal=2,
                            optimize=True,
                            duration=80)
                tosend=attch + '.gif'
            elif subcmd=='vary':
                await message.reply("Command discontinued indefinitely!")
                return
                if os.path.getsize(attch) > 4000000:
                    os.remove(attch)
                    await message.reply('4 MB image size limit')
                    return
                await make_square(attch)
                await openaiAnswer(message, '', attch)
                return
            else:
                wherepls=vars.wherepls
                av = Image.open(attch).resize(wherepls[subcmd][1])
                temp = Image.open(vars.basepath+'/assets/'+wherepls[subcmd][0])
                if subcmd=='love':
                    av.paste(temp, (0, 0), temp)
                    back_im=av
                else:
                    back_im = temp.copy()
                    if subcmd=='boka':
                        d1 = ImageDraw.Draw(back_im)
                        font = ImageFont.truetype(vars.basepath+'/assets/font.ttf', size=40)
                        d1.text((20, 5), str(message.author.name) + ":", fill=(0, 0, 0), font=font)
                        back_im.paste(av, (320, 140), av)
                    else:
                        back_im.paste(av, wherepls[subcmd][2])
                back_im.save(attch + '.png', quality=95)
                tosend=attch + '.png'

            await message.reply(file=discord.File(tosend))
            os.remove(attch)
            os.remove(tosend)
            return

        if self.bot.user.mentioned_in(message) or msg_.startswith('oa ') or msg_=='oa':
            if message.author.id == 736529187724197951 and msg_ == 'oa':
                await message.channel.send('Hajur daju vannus')
                return
            if message.author.id == 843758119065747458 and msg_ == 'oa':
                await message.channel.send('n e r d')
                return
            #if '<@786534057437691914>' in msg_:
            #    quest=''
            #    menrep=message.content.replace('<@786534057437691914>', '')
            #    if message.reference:
            #        ques = await message.channel.fetch_message(message.reference.message_id)
            #        if ques.id in vars.jusnow: return
            #        finalmsg = ques.content.replace('<@786534057437691914>', '')
            #        quest = finalmsg + '\n' + menrep
            #    elif message.content != '<@786534057437691914>':
            #        quest=menrep
            #    if len(quest.strip())>2:
            #        if len(quest)>500:return
            #        # if await mcheck('chatgpt', message): return
            #        if await mcheck('bard', message): return
            #        # await openaiAnswer(message, quest)
            #        await geminiAnswer(message, quest)
            #        return
            if await mcheck('oa', message): return
            if str(message.guild.id) in vars.damnoa:
                if message.content == 'oa':
                    await message.channel.send('Command disabled by your sadist server admin', delete_after=5)
                return
            response = choice(vars.oar)
            try: await message.channel.send(response)
            except: pass
            return

        if msg_=='mim':
            if await mcheck('mim', message): return
            file,mimsent=await sendameme(message, self.u_collection, self.r_collection)
            if not file: return
            dupav = []
            dupav.append(file)

            def checkz(reaction, user):
                if str(reaction.emoji)=="▶️":
                    return user == message.author and reaction.message.id == mimsent.id
                return reaction.message.id == mimsent.id and str(reaction.emoji) in ["👍","👎"]

            while True:
                try:
                    reaction, user = await self.bot.wait_for('reaction_add', check=checkz, timeout=30)
                    if str(reaction.emoji) == '👍' or str(reaction.emoji) == '👎':
                        try: await mimsent.remove_reaction(reaction, user)
                        except: pass
                        continue
                    if len(dupav) > self.duplimit: dupav=[]
                    file, mimsent = await sendameme(message,self.u_collection, self.r_collection, dupav,mimsent)
                    if not file: return
                    dupav.append(str(file))
                    await mimsent.remove_reaction(reaction, user)
                except asyncio.TimeoutError:
                    try: await mimsent.add_reaction('⛔')
                    except: pass
                    try: vars.votex.pop(mimsent.id)
                    except: return
                    return
            return

        if msg_ == 'r!deusi':
            if message.channel.id in self.ingame: return
            if await mcheck('deusi', message): return
            if any(substring in vars.todaynefol.lower() for substring in ['tihar', 'gobardhan', 'bhaai tika']): pass
            else: 
                await message.channel.send("तिहार अल्ली आइसकेको छैन", delete_after=5)
                return
            if message.author.guild_permissions.mute_members or message.author.id == 736529187724197951 or message.channel.id==1173241373302997042: pass
            else:
                await message.channel.send("घर मुली को अनुमती बिना देउसी खेल्न पाइदैन।!\nOnly server MODs are allowed to use this command!", delete_after=5)
                return
                
            self.ingame.append(message.channel.id)
            
            try:
                if message.channel.id==1173241373302997042: await message.channel.send(deusilist[0].replace('@here',''))
                else: await message.channel.send(deusilist[0])
                await message.channel.send(choice(deusilist[1]))
                await message.channel.send(deusilist[2])
            except: 
                self.ingame.remove(message.channel.id)
                return
                
            i=2 
            lassentme=datetime.now()
            while True:
                i+=1
                def check(m):
                    return m.channel == message.channel and any(char in m.content.lower() for char in ['deus','vail', 'bhail', 'देउस', 'भैल'])
                try:
                    msg = await self.bot.wait_for('message', check=check, timeout=300)
                except asyncio.TimeoutError:
                    try: await message.reply('Timeout!', delete_after=20)
                    except: pass
                    break
                if msg.content == 'r!deusi':
                    if msg.author.id==message.author.id or msg.author.id == 736529187724197951: # msg.author.guild_permissions.mute_members or msg.author.id == 736529187724197951 or msg.channel.id==1173241373302997042:
                        try: 
                            await message.channel.send(deusilist[deusilen-2])
                            await message.channel.send(deusilist[deusilen-1])
                        except: pass
                        # try: await msg.add_reaction('👌')
                        # except: pass
                        break
                while True:
                    if (datetime.now()-lassentme).seconds>=3: break
                    await asyncio.sleep(1)  
                try: 
                    await message.channel.send(deusilist[i])
                    if i==deusilen-2:
                        await message.channel.send(deusilist[i+1])
                        break
                except: break
                lassentme=datetime.now()
            self.ingame.remove(message.channel.id)
            return

        if vars.listennow:
            uid=message.author.id
            if self.lastuser==uid: return
            else: self.lastuser=uid
            if uid in vars.blacklisted: return
            if message.guild.id in vars.blacklisted: return
            self.acount+=1
            self.update_server_data(message.guild.id, message.guild.name)
            if self.acount>=20:
                servlist=self.servlist.copy()
                for server_id, data in servlist.items():
                    existing_server = self.s_collection.find_one({'id': server_id})
                    if existing_server:
                        updated_score = existing_server['score'] + data['score']
                        self.s_collection.update_one({'id': server_id}, {'$set': {'score': updated_score}})
                    else:
                        self.s_collection.insert_one({"id": server_id, "name": data["serverName"], "score": data['score']})
                self.servlist.clear()  
                self.acount=0
                self.lastuser=0
                vars.listennow=None


async def setup(bot):
    await bot.add_cog(MessagesCog(bot))
