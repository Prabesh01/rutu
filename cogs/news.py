import discord
from discord.ext import commands
from discord.ext import tasks
import vars
import requests, json
from bs4 import BeautifulSoup
import re
from random import choice

import os

import pymongo

import datetime
import calendar
import pytz
tz_NP = pytz.timezone('Asia/Kathmandu')

import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

ghurl = os.getenv("Github_gist_URL")
ghheader = {'Authorization': f'token {os.getenv("gist_token")}'}


singless=['bbc.com','gorkhapatraonline.com','ekantipur.com','kathmandupost.com','risingnepaldaily.com','annapurnapost.com','onlinekhabar.com','lokaantar.com','ratopati.com','setopati.com','ujyaaloonline.com','ekagaj.com','mysansar.com','pahilopost.com''reportersnepal.com','baahrakhari.com','kharibot.com','ukeraa.com','swasthyakhabar.com','ictsamachar.com','techpana.com','karobardaily.com','arthadabali.com','arthasansar.com','hamrokhelkud.com']
majorsources=['ujyaaloonline.com','setopati.com','lokaantar.com','annapurnapost.com','bbc.com','english.onlinekhabar.com','ekantipur.com','gorkhapatraonline.com','ukeraa.com','kathmandupost.com','nayapatrikadaily.com','techsathi.com','neplays.com','corporatenepal.com','arthasansar.com','ajakoartha.com','swasthyakhabar.com','karobardaily.com','nepallive.com']
teches=['ictsamachar.com','techlekh.com','techpana.com','techsathi.com']

async def getfooter(t_collection):
    try:
        embabe = ''
        if os.path.exists(vars.basepath + '/out/footerr'):
            with open(vars.basepath + '/out/footerr', 'r') as ft:
                embabe = ft.read()
        now = datetime.datetime.now(tz_NP)
        last_day = calendar.monthrange(now.year, now.month)[1]
        if now.day == last_day:
            winner_servers = list(t_collection.find().sort('score', pymongo.DESCENDING))
            winserv=', '.join([serv['name'] for serv in winner_servers])
            embabe=f"Top 5 active guilds for the month of {vars.lastmonth}: {winserv}."
        if choice(range(0, 3)) == 1:return embabe
        else: return ''
    except Exception as e:
        print('getfooter err - '+str(e))
        return ''


class newscog(commands.Cog, name="news"):
    def __init__(self, bot):
        self.bot = bot
        self.lastone=''
        self.t_collection = self.bot.db.t_collection

    def cog_unload(self):
        self.send_news.cancel()

    @tasks.loop(minutes=2)
    async def send_news(self):
        try:
            bin = json.loads(requests.get(ghurl).json()['files']['rnews.json']['content'])
            page = requests.get("https://www.hamropatro.com/news", verify=False)
            soup = BeautifulSoup(page.content, "html.parser")
            newsdivs = soup.findAll("div", class_ = "newsInfo")
            newsdiv = newsdivs[0]

            title=newsdiv.a.contents[0]
            summary=newsdiv.find("div", class_ = "newsSummary").text
            newslinks = newsdiv.findAll("a")
            link= re.sub('/r/','',re.search('/r/.*',newslinks[1].attrs['href']).group())
            if self.lastone!=link:
                self.lastone=link
                embed=discord.Embed(title=title, description=summary, url=link, color = 0x2596be)
                for auth in vars.ss.keys():
                    if auth in link:
                        break
                embed.set_author(name=vars.ss[auth][0], url='https://'+auth, icon_url=vars.ss[auth][1])
                embed.set_footer(text=await getfooter(self.t_collection))
                for chan in [*bin.values()]:
                    try:
                        if chan==997503755283083314: 
                            if not  any(srce == auth for srce in majorsources): continue
                        if chan==1288080181080358943:
                            if not  any(srce == auth for srce in teches): continue
                        channel=self.bot.get_channel(chan)
                        await channel.send(embed=embed)
                    except: pass
        except Exception as e: print('send_news err - ' + str(e))

    @commands.Cog.listener()
    async def on_ready(self):
        self.send_news.start()


async def setup(bot):
    await bot.add_cog(newscog(bot))
