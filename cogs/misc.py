import discord
from discord import app_commands
from discord.ext import commands
import time
import vars
from cogs.voice import slashchecker

async def accesscheck(interaction: discord.Interaction) -> bool:
    return interaction.user.id==736529187724197951


class MiscCogs(commands.Cog, name="misc"):
    def __init__(self, bot):
        self.start_tme = None

    @commands.Cog.listener()
    async def on_ready(self):
        self.start_tme = time.time()

    @app_commands.command(name="report", description="bot owner only: view command usage report")
    @app_commands.check(accesscheck)
    async def report(self, interaction: discord.Interaction):
        wholemsg = f'Uptime: {str(round((time.time() - self.start_tme) / 3600, 3))} hr.\n'
        for i in dict(sorted(vars.trak_.items(), key=lambda item: item[1], reverse=True)):
            wholemsg += f'- ``{i}``  {vars.trak_[i]}.{len(vars.trak_a[i])}.{len(vars.trak_g[i])}\n'
        await interaction.response.send_message(wholemsg)
        return

    @app_commands.command(name="blacklist", description="bot owner only: ban stupid guy from using the bot")
    @app_commands.describe(uid="users' id to blacklist")
    @app_commands.check(accesscheck)
    async def blacklist(self, interaction: discord.Interaction,uid:str=None):
        lis=vars.blacklisted
        if uid:
            if int(uid) in lis:
                vars.blacklisted.remove(int(uid))
                await interaction.response.send_message(f"Unlisted <@{uid}> from blacklist.")
            else:
                vars.blacklisted.append(int(uid))
                await interaction.response.send_message(f"Blacklisted <@{uid}>!")
        else:
            if lis:
                lis = [str(uid) for uid in lis]
                await interaction.response.send_message("<@"+'>, <@'.join(lis)+">")
            else:
                await interaction.response.send_message('N/a')
        return

    @app_commands.command(name="convert", description='currency conversion')
    @app_commands.check(slashchecker)
    @app_commands.allowed_installs(guilds=True, users=True)
    @app_commands.allowed_contexts(guilds=True, dms=True, private_channels=True)
    @app_commands.choices(currency=[
        app_commands.Choice(name="Nepalese Rupee (NPR)", value="npr"),
        app_commands.Choice(name="U.S. Dollar (USD)", value="usd"),
        app_commands.Choice(name="Indian Rupee(INR)", value="inr"),
        app_commands.Choice(name="European Euro (EUR)", value="eur"),
        app_commands.Choice(name="UK Pound Sterling (GBP)", value="gbp"),
        app_commands.Choice(name="Swiss Franc (CHF)", value="chf"),
        app_commands.Choice(name="Australian Dollar (AUD)", value="aud"),
        app_commands.Choice(name="Canadian Dollar (CAD)", value="cad"),
        app_commands.Choice(name="Singapore Dollar (SGD)", value="sgd"),
        app_commands.Choice(name="Japanese Yen (JPY)", value="jpy"),
        app_commands.Choice(name="Chinese Yuan (CNY)", value="cny"),
        app_commands.Choice(name="Saudi Arabian Riyal (SAR)", value="sar"),
        app_commands.Choice(name="Qatari Riyal (QAR)", value="qar"),
        app_commands.Choice(name="Thai Baht (THB)", value="thb"),
        app_commands.Choice(name="UAE Dirham (AED)", value="aed"),
        app_commands.Choice(name="Malaysian Ringgit (MYR)", value="myr"),
        app_commands.Choice(name="South Korean Won (KRW)", value="krw"),
        app_commands.Choice(name="Swedish Krona (SEK)", value="sek"),
        app_commands.Choice(name="Danish Krone (DKK)", value="dkk"),
        app_commands.Choice(name="Hong Kong Dollar (HKD)", value="hkd"),
        app_commands.Choice(name="Kuwaiti Dinar (KWD)", value="kwd"),
        app_commands.Choice(name="Bahraini Dinar (BHD)", value="bhd"),
        ])
    @app_commands.describe(unit="unit")
    async def convert(self, interaction: discord.Interaction, currency: app_commands.Choice[str],unit:float=1.0):
        forex_data = vars.forex_data
        currency=currency.value.upper()

        space_pre=1
        n=12
        diff=1
        even=4
        if currency=="NPR":
            embed_data=f"```{'  #'.ljust(n-(even*2))}|{'  Buy'.ljust(n+even)}|{'  Sell'.ljust(n+even)}\n{'-'*(n*3)}\n"
            embed_title=f"{unit} NPR"
            for xxx, val in forex_data.items():
                buy=(val[0]/val[1])*unit
                sell=(val[0]/val[2])*unit
                embed_data+=f"{' '*space_pre}{xxx.ljust(n-diff-(even*2))}|{' '*space_pre}{str(round(buy,5)).ljust(n-diff+even)}|{' '*space_pre}{str(round(sell,5)).ljust(n-diff+even)}\n"
        else: 
            embed_title=f"{unit} {currency} -> NPR"

            c=forex_data[currency]
            unit_,buy_,sell_=c[0],c[1],c[2]

            buy=(buy_/unit_)*unit
            sell=(sell_/unit_)*unit
            if unit==unit_: embed_data=[[str(unit_)],[str(buy_)], [str(sell_)]]
            else: embed_data=[[str(unit_), str(unit)],[str(buy_), str(round(buy,5))], [str(sell_), str(round(sell,5))]]

            # embed_data+=f"{' '*space_pre}{str(unit_).ljust(n-diff)}|{' '*space_pre}{str(buy_).ljust(n-diff)}|{' '*space_pre}{str(sell_).ljust(n-diff)}\n"
            # if unit!=unit_:
            #     embed_data+=f"{' '*space_pre}{str(unit).ljust(n-diff)}|{' '*space_pre}{str(round(buy,5)).ljust(n-diff)}|{' '*space_pre}{str(round(sell,5)).ljust(n-diff)}\n"
   
        embed = discord.Embed(title=f"__{embed_title}__",color=0x237feb)
        if currency=="NPR":
            embed_data+="```"
            embed.add_field(name=" ", value=embed_data, inline=True)
        else:
            embed.add_field(name="#", value='\n'.join(embed_data[0]), inline=True)
            embed.add_field(name="Buy", value='\n'.join(embed_data[1]), inline=True)
            embed.add_field(name="Sell", value='\n'.join(embed_data[2]), inline=True)

        await interaction.response.send_message(embed=embed)

async def setup(bot):
    await bot.add_cog(MiscCogs(bot))
