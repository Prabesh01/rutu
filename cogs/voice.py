import discord
from discord import app_commands
from discord.ext import commands
from discord import FFmpegPCMAudio
from random import choice
import os
import vars
import json
import nepali_datetime
from datetime import datetime
from datetime import date
import pytz
from discord.ext.commands import Context
import asyncio
from cogs.checks import guard_uncle

tz_NP = pytz.timezone('Asia/Kathmandu')

miti = json.load(open(vars.basepath+'/out/cal.json'))
audiodata = json.load(open(vars.basepath+'/data/play.json'))
swtime=[19,22,27,27,40,34,39,21,24,18,26,29,29,22,41,32,19,34,26,34,24,28,27,24,18,24,17,18,11,18,16]


async def slashchecker(interaction: discord.Interaction) -> bool:
    return await guard_uncle(None,interaction)


class VoiceCogs(commands.Cog, name="voice"):
    def __init__(self, bot):
        self.bot = bot
        self.fmliz={}
        self.autoresume={}

    @commands.Cog.listener()
    async def on_voice_state_update(self,member, before, after):
        if before.channel and before.channel.id in self.fmliz.keys():
            if self.fmliz[before.channel.id] == member.id:
                if before.channel == after.channel: return
                try:
                    self.fmliz.pop(before.channel.id)
                    vc = discord.utils.get(self.bot.voice_clients, channel=before.channel)
                    await vc.disconnect()
                except:
                    pass

    @app_commands.command(name="fm", description="Listen to Live radios, vajan and swosthani")
    @app_commands.check(slashchecker)
    @commands.guild_only()
    @app_commands.choices(choices=[
        app_commands.Choice(name="Ark FM", value="101"),
        app_commands.Choice(name="Radio Nepal", value="1"),
        app_commands.Choice(name="Ujyaalo Radio Network", value="2"),
        app_commands.Choice(name="Radio Thaha Sanchar", value="3"),
        app_commands.Choice(name="Radio Kantipur", value="4"),
        app_commands.Choice(name="Radio Audio", value="5"),
        app_commands.Choice(name="Hits FM", value="6"),
        app_commands.Choice(name="Radio Nagarik", value="7"),
        app_commands.Choice(name="Star FM", value="8"),
        app_commands.Choice(name="Newa FM", value="9"),
        app_commands.Choice(name="Bhorukawa FM", value="10"),
        app_commands.Choice(name="Radio Mithila", value="11"),
        app_commands.Choice(name="BBC world Service", value="12"),
        app_commands.Choice(name="Sanskrit Radio", value="13"),
        app_commands.Choice(name="Mirchi FM", value="14"),
        app_commands.Choice(name="Yesu Radio", value="36"),
        app_commands.Choice(name="Vajan Radio", value="69"),
        app_commands.Choice(name="Swosthani Brata Katha", value="31"),
        ])
    @app_commands.describe(skip_to="Skip to given minute. Applies to swosthani only.")
    async def fm(self, interaction: discord.Interaction, choices: app_commands.Choice[str],skip_to:int=None):
        destination = interaction.user.voice
        if not destination:
            await interaction.response.send_message(f"Join a VC you dumbass",ephemeral=True)
            return
        mynev= interaction.guild.voice_client #interaction.guild.me.voice
        if not mynev:
            mynev=await destination.channel.connect()
            self.fmliz[destination.channel.id] = interaction.user.id
        else:
            if mynev.channel.id!=destination.channel.id:
                await mynev.move_to(destination.channel)
                mynev = interaction.guild.voice_client
                self.fmliz[destination.channel.id] = interaction.user.id
            if mynev.is_playing(): mynev.pause()
        if choices.value=='31':
            for txt in miti:
                if "katha starts" in miti[txt].lower():
                    gibenz = txt.split('-')
                    d0=nepali_datetime.date(int(gibenz[0]), int(gibenz[1]), int(gibenz[2])).to_datetime_date()
                    break
            now = datetime.now(tz_NP)
            d1 = date(now.year, now.month, now.day)
            delta = d1 - d0
            if delta.days < 0 or delta.days > 30:
                if delta.days > 30:
                    await interaction.response.send_message(f"Swosthani Brata Katha ended {str(abs(delta.days) - 30)} days ago")
                    await mynev.disconnect()
                    return
                await interaction.response.send_message(f"{str(abs(delta.days))} days left for swosthani brata katha to begin.")
                await mynev.disconnect()
                return
            duration=swtime[delta.days]
            await interaction.response.send_message(f"Playing Chapter - {str(delta.days + 1)} | ~{duration} min. <:om:934388660793638922>")
            dom = choice(['ia800501', 'ia600501'])
            options=''
            if skip_to:
                options = f'-vn -ss {skip_to*60}'
            mynev.play(FFmpegPCMAudio(f"https://{dom}.us.archive.org/10/items/ShreeSwasthaniBrataKatha_mankoaawaz.blogspot.com/{str(delta.days + 1).zfill(2)}.Mankoaawaz.blogspot.com-ShreeSwasthaniBrataKathaPart{str(delta.days + 1)}.mp3",options=options))
            return
        elif choices.value=='69':
            toplay = vars.basepath + '/assets/baja/' + choice(os.listdir(vars.basepath+"/assets/baja/"))
        else:
            toplay=vars.fmz[choices.value]
        mynev.play(FFmpegPCMAudio(toplay))
        await interaction.response.send_message("👌‎")

    @commands.command(name="play", description='Play songs')
    @commands.guild_only()
    async def rplay(self, ctx: Context):
        if not ctx.author.voice:
            await ctx.send("Join a VC you dumbass",ephemeral=False)
            return
        if str(ctx.guild.id) in vars.damnoa:
            await ctx.send('NSFW commands disabled by your admin. Use r!oa to enable it',ephemeral=True)
            return
        try:
            mynev=await ctx.author.voice.channel.connect()
        except:
            mynev = discord.utils.get(self.bot.voice_clients, guild=ctx.channel.guild)
            await mynev.move_to(ctx.author.voice.channel)
            mynev = discord.utils.get(self.bot.voice_clients, guild=ctx.channel.guild)
            if mynev.is_playing(): mynev.pause()
        mynev.play(FFmpegPCMAudio(vars.basepath + "/assets/tp.mp3"))
        try:
            await ctx.send(':underage:', delete_after=5,ephemeral=False)
            await ctx.send(':exclamation:', delete_after=5,ephemeral=False)
            await ctx.send(':warning:', delete_after=5,ephemeral=False)
        except:
            pass
        while mynev.is_playing(): await asyncio.sleep(.1)
        try:
            await mynev.disconnect()
        except:
            return
        return

    @app_commands.command(name="play", description='Listen to Novel, podcasts and stories.')
    @app_commands.check(slashchecker)
    @commands.guild_only()
    @app_commands.choices(choices=[
        app_commands.Choice(name="Atit ka Panaharu (99)", value="atit_ka_panaharu"),
        app_commands.Choice(name="Summer Love (12)", value="summer_love"),
        app_commands.Choice(name="Saya (11)", value="saya"),
        app_commands.Choice(name="Seto Dharti (20)", value="seto_dharti"),
        app_commands.Choice(name="Karnali Blues (18)", value="karnali_blues"),
        app_commands.Choice(name="Palpasa Cafe (8)", value="palpasa_cafe"),
        app_commands.Choice(name="Aaja Ramita Xa (5)", value="aaja_ramita_xa"),
        app_commands.Choice(name="Arki Aimai (10)", value="arki_aimai"),
        app_commands.Choice(name="Nepal Now (64)", value="nepal_now"),
        app_commands.Choice(name="Boju Bajai (87)", value="boju_bajai"),
        app_commands.Choice(name="Chaubato (66)", value="chaubato"),
        app_commands.Choice(name="Orbiting Human Circus (32)", value="orbitinghumancircus"),
        app_commands.Choice(name="Superego (22)", value="superego"),
        app_commands.Choice(name="Poppy_Hillstead (22)", value="poppy_hillstead"),
        app_commands.Choice(name="TheBlackTapes (31)", value="theblacktapes"),
        app_commands.Choice(name="Ek Purani Kahani (33)", value="ek_purani_kahani"),
        ])
    @app_commands.describe(episode="Choose episode")
    @app_commands.describe(skip_to="Skip to given minute.")
    async def play(self, interaction: discord.Interaction, choices: app_commands.Choice[str],skip_to:int=None,episode:int=None):
        destination = interaction.user.voice
        if not destination:
            await interaction.response.send_message(f"Join a VC you dumbass",ephemeral=True)
            return
        mynev= interaction.guild.voice_client
        if not mynev:
            mynev=await destination.channel.connect()
            self.fmliz[destination.channel.id] = interaction.user.id
        else:
            if mynev.channel.id!=destination.channel.id:
                await mynev.move_to(destination.channel)
                mynev = interaction.guild.voice_client
                self.fmliz[destination.channel.id] = interaction.user.id
            if mynev.is_playing(): mynev.pause()
        options=''
        toplay=audiodata[choices.value]
        totaleps=len(toplay)
        ep=1
        if episode:
            if 1>episode>totaleps: ep=1
            else: ep=episode
        toplay=toplay[ep-1]
        name=list(toplay.keys())[0]
        toplay=toplay[name]
        link=toplay[0]
        duration=toplay[1]

        if interaction.user.id in self.autoresume: aruser=self.autoresume[interaction.user.id]
        else:
            self.autoresume[interaction.user.id]={}
            aruser=self.autoresume[interaction.user.id]
        if not choices.value in aruser: 
            aruser[choices.value]={}
            arpod=aruser[choices.value]
        else: arpod=aruser[choices.value]
        if ep in arpod: 
            if not skip_to:
                skip_to=arpod[ep]//60
                arpod[ep]=60*skip_to
        else: arpod[ep]=-5

        options = ''
        if skip_to:
            if skip_to<=duration:
                arpod[ep]=60*skip_to
                options = f'-vn -ss {skip_to*60}'
        ffmpeg_options = {
            'before_options': f'-user_agent "Mozilla/5.0" {options}'
        }
        await interaction.response.send_message(f"{choices.name.split('(')[0]}: Playing {name} | ~{duration} min")
        mynev.play(FFmpegPCMAudio(link, **ffmpeg_options))
        while mynev.is_playing(): 
            arpod[ep]+=5
            await asyncio.sleep(5)
        try: await mynev.disconnect()
        except: return


async def setup(bot):
    await bot.add_cog(VoiceCogs(bot))
