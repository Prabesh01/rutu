import discord
from discord.ext import commands
from discord import app_commands
from discord.ext.commands import Context
import json
import vars
import requests
from typing import Callable, Optional
import asyncio
import pymongo

import os
ghurl = os.getenv("Github_gist_URL")
ghheader = {'Authorization': f'token {os.getenv("gist_token")}'}
myhook=os.getenv("Log_Webhook_URL")

defembd=[{"title":"No events atm","desc":"May be we will have something for you tomorrow","tv":None,"url":"[Don't Click!](https://www.youtube.com/watch?v=dlPYObP6mkQ)","img":"https://i.imgur.com/tQOuMXT.png"}]

welpcol = vars.welpcol
welpdesc = vars.welpdesc
welpval = vars.welpval
welpkeys=list(welpval)

async def permcheck(ctx):
    if ctx.author.guild_permissions.kick_members or ctx.author.id == 736529187724197951: pass # administrator
    else:
        await ctx.send("Only server mods are allowed to use this command!")
        return True

    member = ctx.guild.me
    perm_list = [perm[0] for perm in ctx.channel.permissions_for(member) if perm[1]]
    if 'manage_webhooks' not in perm_list:
        await ctx.send(
            "Grrrr. I don't have permission to manage webhooks. You must provide me that permission in order to use this command.")
        return True
    return False


async def send_msg(ctx,msg):
    try:
        await ctx.send(msg)
    except:
        await ctx.author.send(f"I couldn't send message in <#{ctx.channel.id}>. Anyways, {msg}")


async def evembed(evz, ctx,title="Routiney Board",desc="Events, updates, announcement and other infos"):
    embed = discord.Embed(title=title, description=desc,color=0x5865F2)
    for ev in evz:
        tit = ev['title']
        if 'drft' in ev.keys():
            if ev['drft']:
                tit='🔴 '+ ev['title']
        embed.add_field(name=tit, value=ev['desc'], inline=False)
        embed.add_field(name="Time and Venue", value=ev['tv'], inline=False)
        embed.add_field(name="Links", value=ev['url'], inline=False)
        if ev['img']:
            embed.set_thumbnail(url=ev['img'])
    await ctx.send(embed=embed)


async def saveevz(ev):
    with open(vars.basepath + '/data/evz.json', 'w') as xdu:
        json.dump(ev, xdu)


class Pagination(discord.ui.View):
    def __init__(self, interaction: discord.Interaction, get_page: Callable):
        self.interaction = interaction
        self.get_page = get_page
        self.total_pages: Optional[int] = None
        self.index = 1
        super().__init__(timeout=100)

    async def interaction_check(self, interaction: discord.Interaction) -> bool:
        if interaction.user == self.interaction.author:
            return True
        else:
            emb = discord.Embed(
                description=f"Only the author of the command can perform this action.",
                color=16711680
            )
            await interaction.response.send_message(embed=emb, ephemeral=True)
            return False

    async def navegate(self):
        emb, self.total_pages = await self.get_page(self.index)
        if self.total_pages == 1:
            await self.interaction.response.send_message(embed=emb)
        elif self.total_pages > 1:
            self.update_buttons()
            await self.interaction.send(embed=emb, view=self)

    async def edit_page(self, interaction: discord.Interaction):
        emb, self.total_pages = await self.get_page(self.index)
        self.update_buttons()
        await interaction.response.edit_message(embed=emb, view=self)

    def update_buttons(self):
        if self.index > self.total_pages // 2:
            self.children[2].emoji = "⏮️"
        else:
            self.children[2].emoji = "⏭️"
        self.children[0].disabled = self.index == 1
        self.children[1].disabled = self.index == self.total_pages

    @discord.ui.button(emoji="◀️", style=discord.ButtonStyle.blurple)
    async def previous(self, interaction: discord.Interaction, button: discord.Button):
        self.index -= 1
        await self.edit_page(interaction)

    @discord.ui.button(emoji="▶️", style=discord.ButtonStyle.blurple)
    async def next(self, interaction: discord.Interaction, button: discord.Button):
        self.index += 1
        await self.edit_page(interaction)

    @discord.ui.button(emoji="⏭️", style=discord.ButtonStyle.blurple)
    async def end(self, interaction: discord.Interaction, button: discord.Button):
        if self.index <= self.total_pages//2:
            self.index = self.total_pages
        else:
            self.index = 1
        await self.edit_page(interaction)

    async def on_timeout(self):
        pass
        # message = await self.interaction.original_response()
        # await message.edit(view=None)

    @staticmethod
    def compute_total_pages(total_results: int, results_per_page: int) -> int:
        return ((total_results - 1) // results_per_page) + 1


class GenCogs(commands.Cog, name="general"):

    def __init__(self, bot):
        self.bot = bot
        self.t_collection = self.bot.db.t_collection

    @commands.hybrid_command(aliases=['ev'], name="event",description='Events!')
    @app_commands.allowed_installs(guilds=True, users=True)
    @app_commands.allowed_contexts(guilds=True, dms=True, private_channels=True)
    async def event(self, ctx: Context,title:str=None,description:str=None,event_link:str=None,time_and_venue:str=None,image_link:str=None,owner_remarks:int=None):
        evz = json.load(open(vars.basepath + '/data/evz.json'))
        if title:
            if owner_remarks and ctx.author.id==736529187724197951: pass
            else:
                newone={}
                newone['title']=title
                newone['desc']=description
                newone['tv']=time_and_venue
                newone['url']=event_link
                newone['img']=image_link
                if ctx.author.id != 736529187724197951:
                    newone['drft'] = True
                evz['events'].append(newone)
                if 'drft' in newone:
                    await evembed([newone], ctx, 'Submitted for Review', "Red circle denotes pending Bot Owner's Approval")
                else:
                    await evembed([newone],ctx,'Added!','Preview')
                await saveevz(evz)
                return
        if ctx.author.id != 736529187724197951:
            nondrft=[d for d in evz['events'] if "drft" not in d]
            if not nondrft:
                nondrft=defembd
            await evembed(nondrft,ctx)
            return
        if owner_remarks:
            if owner_remarks>100:
                owner_remarks-=101
                tochangde= evz['events'][owner_remarks]
                if title: tochangde['title']=title
                if description: tochangde['desc']=description
                if time_and_venue: tochangde['tv']=time_and_venue
                if event_link: tochangde['url']=event_link
                if image_link: tochangde['img']=image_link
                await evembed([evz['events'][owner_remarks]], ctx, 'Edited!','Preview')
            elif owner_remarks>0:
                evz['events'][owner_remarks-1].pop('drft')
                await evembed([evz['events'][owner_remarks-1]], ctx, 'Approved!','Preview')
            else:
                owner_remarks*=-1
                await evembed([evz['events'][owner_remarks-1]], ctx, 'Removed!','Preview')
                evz['events'].pop(owner_remarks-1)
            await saveevz(evz)
            return
        else:
            nondrft=evz['events']
            if not nondrft:
                nondrft=defembd
            await evembed(nondrft, ctx)
            return

    @commands.command(aliases=['inv'], name="invite", description='Get Bot\'s Invite Link')
    async def inv(self, ctx: Context):
        await ctx.send('https://discord.com/application-directory/786534057437691914')

    @commands.command(name="vote", description='Plis vote my bot')
    async def vote(self, ctx: Context):
        await ctx.send('<https://top.gg/bot/786534057437691914/vote/>')

    @commands.command(name="ping", description='ping dudes')
    async def ping(self, ctx: Context, count=1):
        if ctx.author.id!=736529187724197951: return
        members = ctx.guild.members
        i=0
        lis=[]
        for member in members:
            if member.bot:continue
            i+=1
            lis.append(str(member.id))
            if i%5==0:
                await ctx.send(f'<@{"> <@".join(lis)}>')
                lis=[]
                if int(i/5)==count: break
                await asyncio.sleep(1)

    @commands.command(name="faq", description='Foire Aux Question')
    async def faq(self, ctx: Context):
        embed = discord.Embed(color=0x206694)
        embed.add_field(name="RONB ko official bot ho?", value="haina", inline=False)
        embed.add_field(name="post ko link pani send gare ramro hunthyo",
                        value="post mathi Routine of Nepal banda lekheko ma click", inline=False)
        embed.add_field(name="Bot ko source code?",
                        value="https://gitlab.com/Prabesh01/rutu", inline=False)
        embed.add_field(name="How many servers are using this bot?",
                        value="As of now, " + str(len(self.bot.guilds)) + " servers have this bot.", inline=False)
        embed.add_field(name="How to donate?", value="chaidaina", inline=False)
        embed.add_field(name="Some messages are splitted, why?",
                        value="Eutai message ma 500 vanda badi nepali characters vayo vane discord le last tira ko words ko आकार उकार faldinxa. So long nepali posts are divided into multiple messages.\nMore info: https://support.discord.com/hc/en-us/community/posts/1500001279881",
                        inline=False)
        embed.add_field(name="kaile kai post dheelo auxa",
                        value="facebook le request block garxa kaile kai",
                        inline=False)
        embed.add_field(name="Yesari scrape gareko illegal hoina?", value="ho", inline=False)
        embed.set_footer(text="Contact _prabesh if you faced any issue with the bot\nSupport Server: ehRSPHuTDy")
        await ctx.send(embed=embed)

    @commands.hybrid_command(aliases=['sub'], name="subscribe",description='Subscribe to RONB posts')
    @commands.guild_only()
    async def sub(self, ctx: Context):
        if await permcheck(ctx):return
        bin = json.loads(requests.get(ghurl).json()['files']['blob.json']['content'])
        hooks = bin['hooks']
        channl = bin['channel']
        channel = ctx.channel
        clts = str(channel.id)
        if clts not in channl:
            channl.append(clts)
            try: web = await channel.create_webhook(name='Routiney')
            except: await send_msg(ctx,f"Couldn't create a webhook in <#{ctx.channel.id}>")
            clt = web.url
            data = {'content': 'subd: ' + str(clts) + ' : ' + str(clt)}
            requests.post(myhook, data=data)
            hooks.append(clt)
            requests.patch(ghurl, data=json.dumps({'files': {'blob.json': {"content": json.dumps(bin)}}}),
                           headers=ghheader)
            await send_msg(ctx,f"<#{ctx.channel.id}> is subscribed to receive routiney's new shit. Wait till RONB posts any new post.\nUse r!unsub to unsubscribe\nFeel free to contact _prabesh if you face any issue.")
        else:
            await send_msg(ctx,f'<#{ctx.channel.id}> was already subscribed. \n Use r!unsub to unsubscribe first.')

    @commands.hybrid_command(aliases=['unsub'], name="unsubscribe",description='Unsubscribe from RONB posts')
    @commands.guild_only()
    async def unsub(self, ctx: Context):
        if await permcheck(ctx): return
        bin = json.loads(requests.get(ghurl).json()['files']['blob.json']['content'])
        hooks = bin['hooks']
        channl = bin['channel']
        channel = ctx.channel
        clts = str(channel.id)
        if clts in channl:
            for hook in await channel.webhooks():
                if hook.user==self.bot.user and hook.name == "Routiney":
                    if hook.url in hooks:
                        data = {'content': 'unsubd: ' + clts + ' : ' + hook.url}
                        requests.post(myhook, data=data)
                        hooks.remove(hook.url)
                        await hook.delete()
            channl.remove(clts)
            requests.patch(ghurl, data=json.dumps({'files': {'blob.json': {"content": json.dumps(bin)}}}),
                           headers=ghheader)
            await send_msg(ctx,f'<#{ctx.channel.id}> is unsubscribed sucessfully\nFeel free to send me your feedbacks/complaints: _prabesh')
        else:
            await send_msg(ctx,f"<#{ctx.channel.id}> was never subscribed\nUse r!sub to subscribe")

    @commands.hybrid_command(name="repost",description='Enable/Disable repost feature')
    @commands.guild_only()
    async def repost(self, ctx: Context):
        if await permcheck(ctx): return
        rpst = json.loads(requests.get(ghurl).json()['files']['blobre.json']['content'])
        if str(ctx.guild.id) in rpst:
            for hook in await ctx.channel.webhooks():
                if hook.name == 'Routiney repost':
                    try:await hook.delete()
                    except:pass
            rpst.pop(str(ctx.guild.id))
            requests.patch(ghurl, data=json.dumps({'files': {'blobre.json': {"content": json.dumps(rpst)}}}),
                           headers=ghheader)
            await send_msg(ctx,f'Sucessfully disabled Repost in <#{ctx.channel.id}>\nUse r!repost in any channel where you want members to be able to repost Routiney\'s messages; mostly a chat channel where a member can talk about the news with others')
        else:
            try: web = await ctx.channel.create_webhook(name='Routiney repost')
            except: await send_msg(ctx,f"Couldn't create a webhook in <#{ctx.channel.id}>")
            rpst[ctx.guild.id] = web.url
            requests.patch(ghurl, data=json.dumps({'files': {'blobre.json': {"content": json.dumps(rpst)}}}),
                           headers=ghheader)
            await send_msg(ctx,f'Repost enabled in <#{ctx.channel.id}>!\nWhenever a member reacts repost in Routiney\'s messages, the messages will be reposted here.\nNews channel are mostly muted by everyone, so if a member wants to talk about a news in another channel, this feature comes in handy\nUse r!repost to disable this')
        return

    @commands.hybrid_command(name="oa",description='Enable/Disable NSFW commands')
    @commands.guild_only()
    async def oa(self, ctx: Context):
        if await permcheck(ctx): return
        oabin={}
        if str(ctx.guild.id) in vars.damnoa:
            vars.damnoa.remove(str(ctx.guild.id))
            await send_msg(ctx,'oa command enabled sucessfully')
        else:
            vars.damnoa.append(str(ctx.guild.id))
            await send_msg(ctx,'oa command disabled sucessfully')
        oabin['oa'] = vars.damnoa
        requests.patch(ghurl, data=json.dumps({'files': {'bloboa.json': {"content": json.dumps(oabin)}}}),
                       headers=ghheader)

    @commands.hybrid_command(name="news",description='Subscribe/Unsubscribe to news from non-RONB sources')
    @commands.guild_only()
    async def news(self, ctx: Context):
        if await permcheck(ctx): return
        if ctx.guild.id not in vars.hpallow:
            await ctx.send(
                'This command is for certain servers only. Contact _prabesh if you wish to use this command in your server',
                delete_after=5,ephemeral=True)
            return
        bin = json.loads(requests.get(ghurl).json()['files']['rnews.json']['content'])
        if str(ctx.guild.id) not in bin.keys():
            bin[ctx.guild.id]=ctx.channel.id
            data = {'content': 'r!news - subd: ' + str(ctx.guild.id) + ' : ' + str(ctx.channel.id)}
            requests.post(myhook, data=data)
            await send_msg(ctx,f"Subscribed <#{ctx.channel.id}> to receive latest news!")
        else:
            bin.pop(str(ctx.guild.id))
            data = {'content': 'r!news - UNsubd: ' + str(ctx.guild.id)}
            requests.post(myhook, data=data)
            await send_msg(ctx,f'<#{ctx.channel.id}> unsubscribed sucesfully.')
        requests.patch(ghurl, data=json.dumps({'files': {'rnews.json': {"content": json.dumps(bin)}}}),
                       headers=ghheader)

    @commands.hybrid_command(name="help",description='Know your way around the bot')
    async def help(self, ctx: Context):
        async def get_page(page: int):
            head=welpkeys[page-1]
            section=welpval[head]
            emb = discord.Embed(title=head,
                                  description=welpdesc[page-1], color=welpcol[page-1])
            for f in section:
                emb.add_field(name=f, value=section[f], inline=False)

            emb.set_author(name=f"Requested by {ctx.author}")
            emb.set_footer(text=f"{page} / 5 pages")
            return emb, 5

        await Pagination(ctx, get_page).navegate()

    @commands.hybrid_command(name="lb",description='Shows winner servers in monthly leaderboard')
    @app_commands.allowed_installs(guilds=True, users=True)
    async def lb(self, ctx: Context):
        top_servers = list(self.t_collection.find().sort('score', pymongo.DESCENDING))
        tosend = f'Top 5 Most Active Nepali Servers for the month of {vars.lastmonth}:\n'
        if not top_servers: tosend='N/a'
        for idx, server in enumerate(top_servers, start=1):
            tosend += f"**{idx}. **_{server['name']}_ ({server['score']})\n<:pks_reply:915053711213072434> <{server['invite']}>\n"
        await ctx.send(tosend)


    @commands.hybrid_command(name="top",description='Shows top 5 servers in live leaderboard')
    @app_commands.allowed_installs(guilds=True, users=True)
    async def top(self, ctx: Context):
        tosend=''
        top_servers = vars.sorted_collection[:5]
        if not top_servers: tosend='N/a'
        for idx, server in enumerate(top_servers, start=1):
            tosend+=f"**{idx}. **_{server['name']}_ ({server['score']})\n"
        await ctx.send(tosend)

    @commands.hybrid_command(name="server",description="Shows your server's status in leaderboard")
    @commands.guild_only()
    async def server(self, ctx: Context):
        sorted_collection = vars.sorted_collection
        current_rank = None
        previous_rank_data = None
        next_rank_data = None
        for idx, server_data in enumerate(sorted_collection, start=1):
            if server_data['id'] == ctx.guild.id:
                current_rank = idx
                current_rank_data = sorted_collection[idx - 1]
                if idx > 1:
                    previous_rank_data = sorted_collection[idx - 2]
                if idx < len(sorted_collection):
                    next_rank_data = sorted_collection[idx]
        if not current_rank:
            await ctx.send('Your server hasn\'t yet appeared in the leaderboard\nMake sure I have permission to view channels, so that I could provide you points based on your messages')
            return
        tosend=""
        if previous_rank_data:
            tosend=f"**{current_rank-1}. **_{previous_rank_data['name']}_ ({previous_rank_data['score']})\n"
        tosend+=f"**{current_rank}. **_{current_rank_data['name']}_ ({current_rank_data['score']})\n"
        if next_rank_data:
            tosend+=f"**{current_rank+1}. **_{next_rank_data['name']}_ ({next_rank_data['score']})\n"
        await ctx.send(tosend)


async def setup(bot):
    await bot.add_cog(GenCogs(bot))
