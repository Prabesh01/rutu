import discord
from discord.ext import commands
import vars
import requests,json
import os

from discord_webhook import DiscordWebhook, DiscordEmbed

class ReactionsCog(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.r_collection = self.bot.db.r_collection

    @commands.Cog.listener()
    async def on_raw_reaction_add(self, payload):
        emoji = payload.emoji
        if str(emoji) not in ["🔁", "👍", "👎", "🔖","❌"]:
            return
        user = payload.user_id
        if user == self.bot.user.id:return
        message = payload.message_id
        channel = payload.channel_id
        guild = payload.guild_id
        member = payload.member
        votex=vars.votex
        if message in votex:
            rct=None
            if str(emoji) == "👎": rct=-1
            elif str(emoji) == "👍": rct=1
            if rct: self.r_collection.find_one_and_update({"mimid": votex[message], "uid": user},{"$set": {"react": rct}},upsert=True)
            return
        if str(emoji) not in ["🔁", "🔖", "❌"]:
            return
        guil = self.bot.get_guild(guild)
        if not guild and str(emoji) != "❌":
            return
        if guild is None:
            chan = discord.utils.get(self.bot.private_channels, id=channel)
            msz = await chan.fetch_message(message)
            if msz is None: return
            await msz.delete()
            return
        else:
            if user in vars.spammer:
                await payload.member.send("You are being rate limited! Try again after 5 minutes.")
                return
            chan = discord.utils.get(guil.channels, id=channel)
        msz = await chan.fetch_message(message)
        if msz is None: return

        if str(msz.author) != 'Routiney#0000' and guild != 785079398969507880:
            return

        if user != 736529187724197951:
            vars.spammer.append(user)

        if str(emoji) == "🔖":
            if len(msz.embeds) > 0:
                embe = msz.embeds[0]
            else:
                embe = discord.Embed(title="Go to message",
                             url="https://discord.com/channels/" + str(guild) + "/" + str(channel) + "/" + str(message),
                             description=msz.content)
                embe.set_author(name=str(msz.author.name),
                                url="https://discord.com/channels/" + str(guild) + "/" + str(channel) + "/" + str(
                                    message),
                                icon_url=str(msz.author.avatar.url))
            try:
                bookdm = await payload.member.send(embed=embe)
                await bookdm.add_reaction('❌')
            except Exception as e:
                print(e)
            return

        if str(msz.author) != 'Routiney#0000': return

        rpst = json.loads(requests.get(os.getenv("Github_gist_URL")).json()['files']['blobre.json']['content'])
        if not str(guild) in str(rpst.keys()):
            try:
                await chan.send('Well, repost has not been enabled in your server. Admins can use r!repost to enable it.', delete_after=5)
            except:
                await payload.member.send(
                    "Well, repost has not been enabled in your server. Admins can use r!repost to enable it.")
                await payload.member.send(
                    f"Also I don't have permission to send message in <#{channel}>. Context: you reacted repost 🔁.")
            return
        wurl = rpst[str(guild)]
        try:
            attachment_url = msz.attachments[0].url
        except:
            attachment_url = ''
        try:
            embe = msz.embeds[0].to_dict()
            embed = DiscordEmbed(title="Go to message",
                                 url="https://discord.com/channels/" + str(guild) + "/" + str(channel) + "/" + str(
                                     message),
                                 description=str(member.mention) + ' shared a post!')
            webhool = DiscordWebhook(username=str(member.name), avatar_url=str(member.avatar.url), url=wurl,
                                     content=str(msz.content))
            webhool.add_embed(embed)
            webhool.add_embed(embe)
            webhool.execute()
        except:
            webhook = DiscordWebhook(username=str(member.name), avatar_url=str(member.avatar.url), url=wurl)
            embe = DiscordEmbed(description=str(msz.content))
            embe.set_author(name=str(msz.author.name),
                            url="https://discord.com/channels/" + str(guild) + "/" + str(channel) + "/" + str(message),
                            icon_url=str(msz.author.avatar.url))
            embe.add_embed_field(name="\u200b",
                                 value="[Go to message](https://discord.com/channels/" + str(guild) + "/" + str(
                                     channel) + "/" + str(message) + ")", inline=False)
            embe.set_image(url=attachment_url)
            webhook.add_embed(embe)
            webhook.execute()


async def setup(bot):
    await bot.add_cog(ReactionsCog(bot))
