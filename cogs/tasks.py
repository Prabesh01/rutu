import random

import discord
from discord.ext import commands
from discord.ext import tasks
import vars, sys
import json #, requests
import aiohttp
import asyncio

import datetime
import calendar
import pytz
tz_NP = pytz.timezone('Asia/Kathmandu')
midnight = datetime.time(hour=0, minute=1, tzinfo=tz_NP)
off_office = datetime.time(hour=17, minute=1, tzinfo=tz_NP)

import os

import pymongo

from urllib.parse import quote
import topgg
import re

#from bson import ObjectId
#class MongoEncoder(json.JSONEncoder):
#    def default(self, obj):
#        if isinstance(obj, ObjectId):
#            return str(obj)
#        return super(MongoEncoder, self).default(obj)


async def t_response():
    vars.todaynefol = await vars.hp_date_info()
    print('t fecthed!')

async def parse_forex_data():
    d = datetime.datetime.now(tz=tz_NP)
    today = f'{d.year}-{str(d.month).zfill(2)}-{str(d.day).zfill(2)}'
    url=f"https://www.nrb.org.np/api/forex/v1/rates?page=1&per_page=1&from={today}&to={today}"
    async with aiohttp.ClientSession() as session:
        async with session.get(url) as response: json_data = await response.json()

    forex_data={}
    for rate in json_data['data']['payload'][0]['rates']:
        currency=rate["currency"]
        forex_data[currency["iso3"]]=[currency['unit'],float(rate['buy']),float(rate['sell'])]
    vars.forex_data=forex_data


async def fetch_oa():
    async with aiohttp.ClientSession() as session:
        async with session.get(os.getenv("Github_gist_URL")) as response:
            jdata=await response.json()
            oabin = json.loads(jdata['files']['bloboa.json']['content'])
    # oabin = json.loads(requests.get(os.getenv("Github_gist_URL")).json()['files']['bloboa.json']['content'])
    vars.damnoa= oabin["oa"]
    print('oa fetched')


class Tasks(commands.Cog, name="tasks"):
    def __init__(self, bot):
        self.bot = bot
        self.support_server=None
        self.stat=-1
        self.ytvname=self.ytmname=self.usval=self.gaspp=self.tompp=self.nepper=None
        self.s_collection = self.bot.db.s_collection
        self.t_collection = self.bot.db.t_collection

    def cog_unload(self):
        self.spam_reset.cancel()
        self.var_cleanup.cancel()
        self.t_response_task.cancel()
        self.parse_forex_data_task.cancel()
        self.update_stats.cancel()
        self.server_members_fetch.cancel()
        self.leaderboard.cancel()

    @tasks.loop(minutes=1)
    async def spam_reset(self):
        vars.spammer = []
        vars.gptlimit = []
        try:
            self.stat+=1
            if os.path.exists(vars.basepath + '/out/stat'):
                with open(vars.basepath + '/out/stat', 'r') as ft:
                    stname = ft.read()
            elif self.stat==0:
                stname = 'Chilling in '+str(len(self.bot.guilds))+' servers'
            elif self.stat==1:
                if self.ytmname:
                    await self.bot.change_presence(activity=discord.Activity(type=discord.ActivityType.listening, name=self.ytmname))
                return
            elif self.stat==2:
                if self.ytvname: await self.bot.change_presence(activity=discord.Activity(type=discord.ActivityType.watching, name=self.ytvname))
                return
            elif self.stat==3:
                if self.usval: stname = f'Dealing at $1=NRs. {self.usval}'
                else: return
            elif self.stat==4:
                if self.gaspp: stname = f'Paying {self.gaspp} for gas'
                else: return
            elif self.stat==5:
                if self.tompp: stname = f'Bargaining over {self.tompp}/kg tomato'
                else: return
            else:
                self.stat=-1
                if not self.nepper: return
                st='decrease'
                if float(self.nepper)>0: st='increase'
                stname = f'Observing a {abs(float(self.nepper))}% {st} in NEPSE'
            await self.bot.change_presence(activity=discord.CustomActivity(name=stname))
        except Exception as e: print('spam_reset err - '+str(e))

    @tasks.loop(minutes=40)
    async def leaderboard(self):
        print('chkpnt lb start')
        try:
            await t_response()
            now = datetime.datetime.now(tz_NP)
            last_day = calendar.monthrange(now.year, now.month)[1]

            vars.sorted_collection = list(self.s_collection.find().sort('score', pymongo.DESCENDING))
            #if os.path.isdir('/var/www/html/'):
            #    with open('/var/www/html/top.json', 'w', encoding='utf-8') as json_file: json.dump(vars.sorted_collection, json_file, cls=MongoEncoder)
            if not vars.sorted_collection:
                vars.listennow = 1
                return
            top_server_1_score = vars.sorted_collection[0]['score']
            winner_server_1_score=-1

            if now.day == last_day:
                print('todays last day of the month')
                vars.lastmonth = now.strftime("%B")

                winner_servers = list(self.t_collection.find().sort('score', pymongo.DESCENDING))

                if winner_servers:
                    winner_server_1_score = winner_servers[0]['score']

                if top_server_1_score!=winner_server_1_score:
                    print('doing it')
                    top5=vars.sorted_collection[:5]
                    invites={}
                    server_icons={}
                    for top in top5:
                        invites[top['id']]='https://discord.gg/-'
                        server_icons[top['id']]=''
                        #r=requests.get(f'https://top.gg/api/search?q={quote(top["name"])}&limit=108').json()
                        #for s in r['results']['servers']:
                        #    if str(s['id'])==str(top['id']):invites[top['id']]='https://top.gg/servers/'+str(top['id'])
                        #    break

                    for guild in self.bot.guilds:
                        if guild.id in invites:
                            try: server_icons[guild.id]=guild.icon.url
                            except: print("Couldn't fetch server icon")
                            if invites[guild.id]!='https://discord.gg/-': continue
                            for channel in guild.channels:
                                try:
                                    iinv = await channel.create_invite(max_age=0,unique=False)
                                    invites[guild.id]=iinv.url
                                    break
                                except: pass

                    self.t_collection.delete_many({})
                    for top in top5:
                        self.t_collection.insert_one({"id": top['id'], "name": top["name"], "score": top['score'],"invite":invites[top['id']], "icon":server_icons[top['id']]})

                return
            else:
                vars.lastmonth=(now.replace(day=1) - datetime.timedelta(days=1)).strftime("%B")
            if now.day==1:
                print('todays first day of the month')

                winner_servers = list(self.t_collection.find().sort('score', pymongo.DESCENDING))
                if winner_servers:
                    winner_server_1_score = winner_servers[0]['score']

                if top_server_1_score==winner_server_1_score:
                    print('doing it')
                    self.s_collection.delete_many({})
                    vars.sorted_collection=[]

            vars.listennow = 1
        except Exception as e: print('leaderboard err - '+str(e))
        print('chkpnt lb end')


    @tasks.loop(minutes=1440)
    async def server_members_fetch(self):
        print('chkpnt sf start')
        try:
            if not self.support_server: return
            server_members={}
            async for member in self.support_server.fetch_members(limit=None):
                if member.bot: continue
                server_members[member.id]=[]
                for guil in member.mutual_guilds:
                    if guil.id == self.support_server.id: continue
                    server_members[member.id].append({guil.id:guil.name})

            with open(vars.basepath + '/out/server_members.json', 'w') as xdu:
                json.dump(server_members, xdu)
        except Exception as e: print('server_members_fetch err - '+str(e))
        print('chkpnt sf end')


    @tasks.loop(minutes=5)
    async def var_cleanup(self):
        vars.dmerz = []
        vars.jusnow = []

    @tasks.loop(time=off_office)
    async def parse_forex_data_task(self):
        try: await parse_forex_data()
        except Exception as e: print('parse_forex err - '+str(e))

    @tasks.loop(time=midnight)
    async def t_response_task(self):
        try: await t_response()
        except Exception as e: print('t_response err - '+str(e))

    @tasks.loop(minutes=60)
    async def update_stats(self):
        try:
            async with aiohttp.ClientSession() as session:
                async with session.get('https://www.googleapis.com/youtube/v3/videos?part=localizations&chart=mostPopular&videoCategoryId=10&maxResults=1&regionCode=NP&key='+os.getenv("yt_v3_data_api_key")) as response:
                    r= await response.json()
            # r=requests.get('https://www.googleapis.com/youtube/v3/videos?part=localizations&chart=mostPopular&videoCategoryId=10&maxResults=1&regionCode=NP&key='+os.getenv("yt_v3_data_api_key")).json()
            try:
                locali=r['items'][0]['localizations']
                self.ytmname=locali[list(locali.keys())[0]]['title']
            except: pass

            async with aiohttp.ClientSession() as session:
                async with session.get('https://www.googleapis.com/youtube/v3/videos?part=localizations&chart=mostPopular&maxResults=2&regionCode=NP&key='+os.getenv("yt_v3_data_api_key")) as response:
                    r= await response.json()
            # r=requests.get('https://www.googleapis.com/youtube/v3/videos?part=localizations&chart=mostPopular&maxResults=2&regionCode=NP&key='+os.getenv("yt_v3_data_api_key")).json()
            try:
                locali=r['items'][0]['localizations']
                self.ytvname=locali[list(locali.keys())[0]]['title']
            except: pass
            if self.ytvname==self.ytmname:
                try:
                    locali=r['items'][1]['localizations']
                    self.ytvname=locali[list(locali.keys())[0]]['title']
                except: pass

            async with aiohttp.ClientSession() as session:
                async with session.get('https://www.nrb.org.np/api/forex/v1/rate') as response:
                    jdata=await response.json()
                    self.usval = jdata['data']['payload']['rates'][1]['buy']
            # self.usval=requests.get('https://www.nrb.org.np/api/forex/v1/rate').json()['data']['payload']['rates'][1]['buy']

            async with aiohttp.ClientSession() as session: 
                async with session.get('https://noc.org.np/retailprice') as r:
                   #r=requests.get('https://noc.org.np/retailprice')
                   try: self.gaspp=re.findall('<table.*?</table>',await r.text(),re.DOTALL)[0].split('<tr>')[2].split('<td>')[3].split('</td>')[0].strip()
                   except: pass
           
            #r=requests.get('https://kalimatimarket.gov.np/price')
            async with aiohttp.ClientSession() as session:
                async with session.get('https://kalimatimarket.gov.np/price') as r:
                    try: self.tompp=int(re.findall('गोलभेडा.*?(?=</tr>)',await r.text())[0].split('<td>')[-1].split('.')[0][3:])
                    except: pass
            async with aiohttp.ClientSession() as session:
                async with session.get('https://nepalstock.onrender.com/nepse-index') as response:
                   jdata=await response.json(content_type=None)
                   for i in jdata:
                       if i["index"]=="NEPSE Index": self.nepper=i['perChange']
            # self.nepper=requests.get('https://nepsealpha.com/ajax/live-scan-nepse',headers={"User-Agent":"Mozilla/5.0"}).json()['percent_change']
        except Exception as e: print('update_stats err - '+str(e))
        try:
            await self.bot.topggpy.post_guild_count()
        except Exception as e:
            print('Failed to post server count\n{}: {}'.format(type(e).__name__, e))

    @commands.Cog.listener()
    async def on_ready(self):
        self.spam_reset.start()
        self.var_cleanup.start()
        self.bot.topggpy = topgg.DBLClient(self.bot, os.getenv("TopGG_dbl_token"))
        self.update_stats.start()
        self.support_server=self.bot.get_guild(785079398969507880)
        self.server_members_fetch.start()
        self.leaderboard.start()
        await t_response()
        await fetch_oa()
        await parse_forex_data()
        self.t_response_task.start()
        self.parse_forex_data_task.start()


async def setup(bot):
    await bot.add_cog(Tasks(bot))
