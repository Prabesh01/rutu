import discord
from discord.ext import commands

import os

import requests


class GuildJoinCog(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.Cog.listener()
    async def on_guild_join(self,guild):
        data = {'content': 'joined: ' + str(guild.id) + ' : ' + str(guild.name)}
        requests.post(os.getenv("Log_Webhook_URL"), data=data)
        for channel in guild.text_channels:
            if channel.permissions_for(guild.me).send_messages:
                await channel.send("Use r!sub command to start receiving latest RONB posts to your server")
                member = guild.me
                perm_list = [perm[0] for perm in channel.permissions_for(member) if perm[1]]
                if not ('embed_links' in perm_list):
                    await channel.send(
                        "It would be great if I had permission to embed links. Anyways, here you go:\n\nr!help - Display Routiney Bot's Command List\nr!sub - Subscribe this text channel to receive new RONB posts\nr!unsub- Unsubscribe this text channel\n\n- Disable 'oa' command using 'r!oa' if curse words aren't appropritae for your server\n- Contact _prabesh if you faced any issue with the bot or join our support server: ehRSPHuTDy")
                    return
                embed = discord.Embed(title="r!help", description="Display Routiney Bot's Command List")
                embed.add_field(name="r!sub", value="Subscribe this text channel to receive new RONB posts", inline=False)
                embed.add_field(name="r!unsub", value="Unsubscribe this text channel", inline=False)
                embed.set_footer(
                    text="- Disable 'oa' command using 'r!oa' if curse words aren't appropritae for your server\n- Contact _prabesh if you faced any issue with the bot or join our support server: ehRSPHuTDy")
                await channel.send(embed=embed)
                break
            break

    @commands.Cog.listener()
    async def on_guild_remove(self,guild):
        data = {'content': 'Removed: ' + str(guild.id) + ' : ' + str(guild.name)}
        requests.post(os.getenv("Log_Webhook_URL"), data=data)


async def setup(bot):
    await bot.add_cog(GuildJoinCog(bot))
