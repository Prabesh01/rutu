import os

import discord
from discord import app_commands
from discord.ext import commands
from discord.ext.commands import Context
import vars
import nepali_datetime
import requests,json

import datetime
tz_NP = datetime.timezone(datetime.timedelta(hours=5, minutes=45))

miti = json.load(open(vars.basepath+'/out/cal.json'))

# import sys
# import os
# sys.path.append(os.path.dirname(os.path.abspath(__name__)))
from cogs.on_message import eventbuilder
from cogs.voice import slashchecker


class CalendarCog(commands.Cog, name="calendar"):
    def __init__(self, bot):
        self.bot = bot

    @app_commands.command(name="today",description='Whats today? Which year is this?')
    @app_commands.check(slashchecker)
    @app_commands.allowed_installs(guilds=True, users=True)
    @app_commands.allowed_contexts(guilds=True, dms=True, private_channels=True)
    async def today(self, interaction: discord.Interaction):
        now = datetime.datetime.now(tz_NP)
        todaynefol=vars.todaynefol
        today_info=todaynefol.split('\n')
        np_date=today_info[0].split(',')[0].replace(':flag_np:', '').strip()
        eng_date=today_info[2]
        if not ':date:' in eng_date: eng_date=today_info[3]
        eng_date=eng_date.replace(':date:', '').strip()

        await interaction.response.send_message(todaynefol +await eventbuilder(np_date,eng_date)+ '\n:clock3: ' + now.strftime("%I:%M.%S %p"))

    @app_commands.command(name="cal", description="lookup Calendar")
    @app_commands.check(slashchecker)
    @app_commands.allowed_installs(guilds=True, users=True)
    @app_commands.allowed_contexts(guilds=True, dms=True, private_channels=True)
    @app_commands.describe(date="Lookup a Date YYYY-MM-DD (B.S.)", search="Search an Event")
    async def cal(self, interaction: discord.Interaction, date: str=None, search: str=None):
        if not date and not search:
            await interaction.response.send_message(file=discord.File(vars.basepath+'/out/cal.png'), ephemeral=True)
            return
        if date and search and interaction.user.id==736529187724197951:
            tocheck=vars.basepath+'/data/events/'+date
            if len(date)>5:
                if os.path.exists(tocheck):
                    os.remove(tocheck)
                else:
                    with open(tocheck,'w',encoding='utf-8') as f: f.write(search+'\n')
            events=os.listdir(vars.basepath+'/data/events/')
            events.remove('.gitignore')
            tosend=''
            for event in events:
                tosend+=f"\n**{event}**\n"+open(vars.basepath + '/data/events/' + event, "r").read()
            await interaction.response.send_message(
                tosend, ephemeral=True)
            return
        if date:
            giben=date.split('-')
            if len(giben) != 3:
                await interaction.response.send_message(
                    "Invalid Date Format!\nRequired Format is: 'YYYY-MM-DD' in B.S.", ephemeral=True)
                return
            if giben[0].isnumeric() and giben[1].isnumeric() and giben[2].isnumeric():
                try:
                    newmew = nepali_datetime.date(int(giben[0]), int(giben[1]),int(giben[2]))
                    nepdatenew = newmew.strftime("%d %B %Y, %G")
                    engdatenew = nepali_datetime.date(int(giben[0]), int(giben[1]),int(giben[2])).to_datetime_date().strftime("%b %d, %Y")
                    urldatenew = '{d.year}-{d.month}-{d.day}'.format(d=newmew)
                except Exception as e:
                    await interaction.response.send_message('Invalid Argument', ephemeral=True)
                    return
                rwsnew = requests.get('https://www.hamropatro.com/date/' + urldatenew).text
                rawtitnew = rwsnew[rwsnew.find('<title>') + 7: rwsnew.find('</title>')]
                if rawtitnew.split(' | ')[1] == '  ':
                    await interaction.response.send_message('Mind your argument!', ephemeral=True)
                    return
                eviz=await eventbuilder(nepdatenew, engdatenew)
                if 'Nepali Calender' in rawtitnew:
                    await interaction.response.send_message(f":flag_np: {nepdatenew}\n:date: {engdatenew}{eviz}")
                    return
                titisnew = rawtitnew.split(' | ')[0] + ' | ' + rawtitnew.split(' | ')[1]
                await interaction.response.send_message(
                    f":flag_np: {nepdatenew}\n:page_facing_up: {titisnew}\n:date: {engdatenew}{eviz}")
            else:
                await interaction.response.send_message(
                    "Invalid Date Format!\nRequired Format is: 'YYYY-MM-DD' in B.S.", ephemeral=True)
                return
            return
        if 3 > len(search) > 47:
            await interaction.response.send_message('Argument should be between 3 to 47 characters', ephemeral=True)
        search = search.lower()
        matches=[]
        for txt in miti:
            if search in miti[txt].lower():
                gibenz=txt.split('-')
                newmew = nepali_datetime.date(int(gibenz[0]), int(gibenz[1]), int(gibenz[2]))
                eewmew = newmew.to_datetime_date()
                eviz=await eventbuilder(newmew.strftime("%d %B %Y, %G"), eewmew.strftime("%b %d, %Y"))
                matches.append(f':date: {txt} | {eewmew.strftime("%Y-%m-%d, %A")}\n:page_facing_up: {miti[txt]}{eviz}')
        if matches:
            matchlen=len(matches)
            if matchlen==1:
                tosend=f"**1 result found for {search}!**\n{matches[0]}"
            else:
                tosend=f"**{matchlen} results found for {search}!**\n"+'\n----\n'.join(matches)
        else:
            tosend='N/a'
        if len(tosend)>2000:
            for i in range(round(len(tosend)/1500)):
                if i==0:
                    await interaction.response.send_message(tosend[0:1500])
                else:
                    await interaction.followup.send(tosend[1500 * i:(1500 * i)+1500])
        else:
            await interaction.response.send_message(tosend)


async def setup(bot):
    await bot.add_cog(CalendarCog(bot))
