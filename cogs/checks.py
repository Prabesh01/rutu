import vars


async def guard_uncle(ctx=None, interaction=None, cmd=None, msg=None):
    if msg:
        ctx=msg
    if interaction:
        user=interaction.user
        ctx=interaction
    else:
        user = ctx.author
    if not cmd:
        cmd = ctx.command.qualified_name
    if ctx.guild:
        gid = ctx.guild.id
    else: gid = 10
    uid=user.id

    if cmd in vars.trak_.keys():
        vars.trak_[cmd] = vars.trak_[cmd] + 1
        if uid not in vars.trak_a[cmd]:
            vars.trak_a[cmd].append(uid)
        if gid not in vars.trak_g[cmd]:
            vars.trak_g[cmd].append(gid)
    else:
        vars.trak_[cmd] = 1
        vars.trak_a[cmd] = [uid]
        vars.trak_g[cmd] = [gid]

    if uid in vars.blacklisted: return False
    if gid in vars.blacklisted: return False
    return not user.bot
