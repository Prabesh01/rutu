import discord
from discord.ext import commands
from discord import app_commands
import os
import vars
import requests, json
from PIL import Image, ImageDraw, ImageFont
from cogs.voice import slashchecker

async def seeresult(interaction, symbol, dob):
    try:
        r = requests.post('http://result.see.gov.np/Result/Index',
                          headers={"Content-Type": "application/x-www-form-urlencoded; charset=UTF-8"},
                          data=f"model%5BAcademicYearId%5D=1&model%5BDateOfBirthBS%5D={dob}&model%5BSymbolNo%5D={symbol}&model%5BAcademicYears%5D%5B0%5D%5BId%5D=1&model%5BAcademicYears%5D%5B0%5D%5BDescription%5D=2078+(+78+)")
        rj = json.loads(r.text)
        if not rj['Data']['StudentName']:
            await interaction.response.send_message.send("Invalid Symbol Number or DOB!", ephemeral=True)
            return

        back_bok = Image.open(vars.basepath+'/assets/see.png')

        back_bok = back_bok.copy()
        d1 = ImageDraw.Draw(back_bok)

        d1.text((139, 59), symbol, fill=(103, 106, 108), font=ImageFont.truetype(vars.basepath+'/assets/see.ttf', size=15))
        d1.text((139, 36), dob, fill=(103, 106, 108), font=ImageFont.truetype(vars.basepath+'/assets/see.ttf', size=15))
        d1.text((139, 15), rj['Data']['StudentName'], fill=(103, 106, 108),
                font=ImageFont.truetype(vars.basepath+'/assets/see.ttf', size=15))
        d1.text((139, 102), rj['Data']['AcademicYearName'], fill=(103, 106, 108),
                font=ImageFont.truetype(vars.basepath+'/assets/see.ttf', size=15))
        d1.text((139, 80), rj['Data']['SchoolName'], fill=(103, 106, 108),
                font=ImageFont.truetype(vars.basepath+'/assets/see.ttf', size=15))
        for i in range(len(rj['Data']['MarksRecord'])):
            try:
                d1.text((91, 250 + i * 40), rj['Data']['MarksRecord'][i]['SubjectName'], fill=(103, 106, 108),
                        font=ImageFont.truetype(vars.basepath+'/assets/see.ttf', size=15))
            except:
                pass
            try:
                d1.text((455, 248 + i * 40), rj['Data']['MarksRecord'][i]['THOG'], fill=(103, 106, 108),
                        font=ImageFont.truetype(vars.basepath+'/assets/see.ttf', size=20))
            except:
                pass
            try:
                d1.text((570, 248 + i * 40), rj['Data']['MarksRecord'][i]['PROG'], fill=(103, 106, 108),
                        font=ImageFont.truetype(vars.basepath+'/assets/see.ttf', size=20))
            except:
                pass
            try:
                d1.text((680, 248 + i * 40), rj['Data']['MarksRecord'][i]['TotalOG'], fill=(103, 106, 108),
                        font=ImageFont.truetype(vars.basepath+'/assets/see.ttf', size=20))
            except:
                pass
            try:
                d1.text((845, 248 + i * 40), rj['Data']['MarksRecord'][i]['TotalGP'], fill=(103, 106, 108),
                        font=ImageFont.truetype(vars.basepath+'/assets/see.ttf', size=20))
            except:
                pass
        d1.text((845, 570), rj['Data']['GPA'], fill=(103, 106, 108),
                font=ImageFont.truetype(vars.basepath+'/assets/see.ttf', size=20))

        back_bok.save(vars.basepath+'/out/' + str(symbol) + '.png')
        await interaction.response.send_message(file=discord.File(vars.basepath+'/out/' + str(symbol) + '.png'), ephemeral=True)
    except:
        try:
            await interaction.response.send_message("Sth went wrong. Try Again!", ephemeral=True)
        except:
            pass
    return


class ContempCog(commands.Cog):
    @app_commands.command(name="see", description="see SEE Results")
    @app_commands.allowed_installs(guilds=True, users=True)
    @app_commands.check(slashchecker)
    @app_commands.describe(symbol="Enter your symbol number", dob="YYYY-MM-DD (B.S.)")
    async def see(self, interaction: discord.Interaction, symbol: str, dob: str):
        symbol = symbol.strip()
        # if dob:
        dob = dob.strip()
        if len(dob.split('-')) != 3:
            await interaction.response.send_message("Invalid Date Format!\nRequired Format is: 'YYYY-MM-DD' in B.S.", ephemeral=True)
            return
        if len(symbol) < 7 or len(symbol) > 9:
            await interaction.response.send_message("Invalid Symbol Number!", ephemeral=True)
            return
        if symbol[-1].isalpha():
            symbol = symbol[:-1]
        if not symbol.isdigit():
            await interaction.response.send_message("Invalid Symbol Number!", ephemeral=True)
            return
        if len(symbol) > 8:
            await interaction.response.send_message("Invalid Symbol Number!", ephemeral=True)
            return
        if os.path.exists(vars.basepath+'/out/' + str(symbol) + '.png'):
            await interaction.response.send_message(file=discord.File(vars.basepath+'/out/' + str(symbol) + '.png'), ephemeral=True)
            return
        await seeresult(interaction, symbol, dob)
        return


async def setup(bot):
    await bot.add_cog(ContempCog(bot))
