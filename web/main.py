from fastapi import BackgroundTasks, Depends, FastAPI, Request, HTTPException, status
from fastapi.templating import Jinja2Templates
import json, os
import secrets
from fastapi.responses import RedirectResponse, FileResponse
from pathlib import Path
from fastapi.staticfiles import StaticFiles

import pymongo
from bson import json_util

from fastapi.security import HTTPBasic, HTTPBasicCredentials
import aiohttp
from dotenv import load_dotenv
load_dotenv()

from fastapi.security import APIKeyHeader
from fastapi import Security
from fastapi.middleware.cors import CORSMiddleware

mongodb_creds=os.getenv("mongodb_creds")
api_key_header = APIKeyHeader(name="X-API-Key")

client = pymongo.MongoClient(f'mongodb://{mongodb_creds}/')
mimdb = client.mim
u_collection=mimdb['users']

lbdb = client.lb
s_collection=lbdb['servers']
t_collection=lbdb['top5']

basepath=os.path.dirname(os.path.abspath(__file__))

f0f_path = Path(basepath+"/404.jpg")

app = FastAPI()
app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

templates = Jinja2Templates(directory=basepath+"/templates")
mim_dir=basepath+"/../data/mim/"
app.mount("/mim", StaticFiles(directory=mim_dir), name="mim")

security = HTTPBasic()

cwdata={}
try:cwdata=json.load(open(basepath+'/../data/web/cwdata.json','r'))
except:pass
crawl_bots = ["intel mac os x 11.6; rv:92.0", "discord","facebookexternalhit","facebookcatalog","facebook.com","facebot","linkedin","twitter","pinterest","whatsapp","googlebot","bingbot","python","aiohttp","httpx","msnbot","crawl","yandex.com","spider","proxy","preview","slackbot","redditbot","searchbot","telegrambot","epicbot","duckduckgo","okhttp","Apache-HttpClient","http_get","check_http","uptimerobot","cloudflare","curl","virustotal","wordpress"]
reserved_keyname="asdsf_ajnfg"
headerz={"User-Agent":"prabesh01@pm.me","key":os.getenv("abuseipdb_api_key")}

def authenticate_user(credentials: HTTPBasicCredentials = Depends(security)):
    is_correct_username = secrets.compare_digest(credentials.username.encode("utf8"), os.getenv("cw_admin").encode("utf-8"))
    is_correct_password = secrets.compare_digest(credentials.password.encode("utf8"), os.getenv("cw_admin").encode("utf-8"))
    return is_correct_username and is_correct_password

def get_cdata():
    vdata=json.load(open(basepath+'/../data/web/vdata.json','r'))
    cdata={}
    for i in cwdata:
        if i in vdata:
            idat=vdata[i]
            cdata[i]=[len(idat[0]),idat[1],idat[2]]
        else:cdata[i]=[0,0,0]
    return cdata

@app.get("/")
async def home_redir():
    return RedirectResponse(url="https://routiney.cote.ws")

@app.get("/cw")
async def cw_form(request: Request,credentials: HTTPBasicCredentials =Depends(authenticate_user)):
    if not credentials:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect email or password",
            headers={"WWW-Authenticate": "Basic"},
        )

    return templates.TemplateResponse("form.html", {"request": request,"data": cwdata,"cnt":len(cwdata),"cdata":get_cdata()})

@app.post("/cw")
async def submit_form(request: Request,credentials: HTTPBasicCredentials= Depends(authenticate_user)):
    if not credentials:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect email or password",
            headers={"WWW-Authenticate": "Basic"},
        )

    global cwdata
    form_data = await request.form()
    chans = form_data.multi_items()
    newdata={}
    for i in range(0, len(chans), 3):
        if chans[i][1]==reserved_keyname: continue
        newdata[chans[i][1]]=[chans[i+1][1],chans[i+2][1]]

    vdata=json.load(open(basepath+'/../data/web/vdata.json','r'))
    keys_to_remove = [i for i in vdata if i != reserved_keyname and i not in newdata]
    for key in keys_to_remove: vdata.pop(key, None)
    with open(basepath+'/../data/web/vdata.json', 'w') as xdu:
        json.dump(vdata,xdu)

    cwdata=newdata
    with open(basepath+'/../data/web/cwdata.json','w') as f:
        json.dump(cwdata,f)

    return templates.TemplateResponse("form.html", {"request": request,"data": cwdata,"cdata":get_cdata()})

async def bottest(cip):
    async with aiohttp.ClientSession() as session:
        async with session.get(f'https://api.abuseipdb.com/api/v2/check?ipAddress={cip}&maxAgeInDays=90', headers=headerz) as response:
            data = await response.json()
            if data['data']['abuseConfidenceScore']>=8: return True
            if any(bb in data['data']['usageType'] for bb in ["Content","Data","Search"]): return True
    return False

async def perform_calculations(cip: str, u_id: str):
    try: vdata=json.load(open(basepath+'/../data/web/vdata.json','r'))
    except: vdata={}
    addbcnt=None
    try:
        if cip in vdata[reserved_keyname]:addbcnt=True
    except: vdata[reserved_keyname]=[]
    if u_id in vdata:
        vdata[u_id][2]+=1
        if addbcnt: vdata[u_id][1]+=1
        elif not cip in vdata[u_id][0]:
            addbcnt=await bottest(cip)
            if addbcnt:
                vdata[u_id][1]+=1
                vdata[reserved_keyname].append(cip)
            else: vdata[u_id][0].append(cip)
    else:
        if not addbcnt:
            addbcnt=await bottest(cip)
        if addbcnt:
            vdata[u_id]=[[],1,1]
            vdata[reserved_keyname].append(cip)
        else: vdata[u_id]=[[cip],0,1]
    with open(basepath+'/../data/web/vdata.json', 'w') as xdu:
        json.dump(vdata,xdu)

@app.get("/api/top5")
async def lb_top5(api_key: str = Security(api_key_header)):
    if api_key != mongodb_creds: raise HTTPException(status_code=403, detail="Invalid API Key")
    cursor=t_collection.find({},{'_id': 0, 'id': 0}).sort('score', pymongo.DESCENDING)
    return json.loads(json_util.dumps(list(cursor)))

@app.get("/api/servers")
async def lb_servers(api_key: str = Security(api_key_header)):
    if api_key != mongodb_creds: raise HTTPException(status_code=403, detail="Invalid API Key")
    cursor=s_collection.find({},{'_id': 0, 'id': 0}).sort('score', pymongo.DESCENDING)
    return json.loads(json_util.dumps(list(cursor)))

@app.get("/api/mims")
async def mims(page: int = 0):
    page_size=20
    skip = (page - 1) * page_size
    cursor = u_collection.find({},{'mimid':1, '_id':0}).sort(
        'date', pymongo.DESCENDING
    ).skip(skip).limit(page_size)
    mimid_list = [doc['mimid'] for doc in cursor]
    return json.loads(json_util.dumps(list(mimid_list)))

@app.get("/{u_id}")
async def redir(request: Request, u_id: str, background_tasks: BackgroundTasks):
    if u_id=="cw": return
    if not u_id in cwdata:
        return FileResponse(f0f_path, media_type="image/jpeg")

    cip=request.client.host
    background_tasks.add_task(perform_calculations, cip, u_id)
    user_agent = request.headers.get("User-Agent", "").lower()
    if any(ua in user_agent for ua in crawl_bots):
        return RedirectResponse(url=cwdata[u_id][0], status_code=302)
    else: return RedirectResponse(url=cwdata[u_id][1], status_code=302)
