## Experimental
Web part doesnt really affect bot's functionality at all so you may simply ignore this directory. Its solely for online presence of the bot.
- `nohup uvicorn main:app --host 0.0.0.0 --port 8000 > ~/botlogs/fastapi.log 2>&1 &`
