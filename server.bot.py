import re

import discord
from discord.ext import tasks
from discord.ext.commands import Bot
from discord import app_commands

import sys
import datetime
import calendar
import pytz
tz_NP = pytz.timezone('Asia/Kathmandu')
import traceback
import requests,json

import os
basepath=os.path.dirname(os.path.abspath(__file__))
from dotenv import load_dotenv
load_dotenv(basepath+'/.env')

from facebook_scraper import get_posts, set_user_agent, _scraper
from PIL import Image
import wget
from random import choice
from discord_webhook import AsyncDiscordWebhook, DiscordEmbed

from langdetect import detect
import xml.etree.ElementTree as ET
import html

import asyncio
#from instagrapi import Client
#cl = Client()
#cl.login(os.getenv("insta_user"), os.getenv("insta_pass"))
ftpmsgfilename='/var/www/mamata/msg.json'


import pymongo
client = pymongo.MongoClient(f'mongodb://{os.getenv("mongodb_creds")}/')
db = client.lb
t_collection = db['top5']

#intents = discord.Intents(members=True,guilds=True)
intents = discord.Intents.default()
intents.members = True
intents.guilds = True
intents.message_content = True
bot = Bot(command_prefix='/', intents=intents)
bot.remove_command('help')

lastone={'ronb_fb':[],'aawaajnews':[],'setopati':[],'onlinekhabar':[],'gorkhapatraonline':[]}
support_server=topmemrole=voterole=newbie=lvlfive=lvlten=lvlfifteen=lvltwenty=lvlthirty=lvlfifty=lvl100=None
lvlroles=[870539276851499008,866246341029527572,866246717875683338,866246844099199066,866247122788679680,975696119864651836,1152420229893324950,1152420666923024384]
defautroles=[906764544423124992,922073024457474058,922066932541308939,963979765168083056,786854817922285588,785105412428267530,884613675334402048,893088506283429908,843841427540148224,869263866054467584,871224668789559346,791304756140179466,902178661606764546,902178859804397579,902179001785810974,934411946214719549, 891330152527695904]
defautroles.extend(lvlroles)


@bot.event
async def on_error(event, *args, **kwargs):
    embed = discord.Embed(title=':x: Role.py - Error', colour=0xe74c3c)
    embed.add_field(name='Event', value=event)
    embed.description = '```py\n%s\n```' % traceback.format_exc()
    embed.timestamp = datetime.datetime.utcnow()
    bot.AppInfo = await bot.application_info()
    await bot.AppInfo.owner.send(embed=embed)


async def getfooter():
    try:
        embabe = ''
        if os.path.exists(basepath + '/out/footer'):
            with open(basepath + '/out/footer', 'r') as ft:
                embabe = ft.read()
        now = datetime.datetime.now(tz_NP)
        last_day = calendar.monthrange(now.year, now.month)[1]
        if now.day == last_day:
            winner_servers = list(t_collection.find().sort('score', pymongo.DESCENDING))
            winserv=', '.join([serv['name'] for serv in winner_servers])
            embabe=f"Top 5 active guilds for the month of {now.strftime('%B')}: {winserv}. Use r!lb to see leaderboard."
        if choice(range(0, 3)) == 1:return embabe
        else: return ''
    except Exception as e:
        print('getfooter err - '+e)
        return ''


async def postwebhook(embed,img=None,thumb=None,footer=None):
    print('sending..')
    try:
        r = json.loads(requests.get(os.getenv("Github_gist_URL")).json()['files']['blob.json']['content'])
        hooks = r['hooks']
        if img:
            #wget.download(img, out=basepath + '/out/post.jpg')
            with open(basepath + '/out/post.jpg', 'wb') as image: image.write(_scraper.get(img).content)
        if thumb:
            wget.download(thumb, out=basepath + '/out/thumb.jpg')
        if footer:
            embed.set_footer(text=await getfooter())

        webhooks = AsyncDiscordWebhook.create_batch(username='Routiney', avatar_url='https://i.imgur.com/tQOuMXT.png', urls=hooks, rate_limit_retry=True)
        for webhook in webhooks:
            if img:
                with open(basepath + '/out/post.jpg', "rb") as f:
                    webhook.add_file(file=f.read(), filename='post.jpg')
                embed.set_image(url="attachment://post.jpg")
            if thumb:
                with open(basepath + '/out/thumb.jpg', "rb") as f:
                    webhook.add_file(file=f.read(), filename='thumb.jpg')
                embed.set_thumbnail(url="attachment://thumb.jpg")
            webhook.add_embed(embed)
            try: 
                await webhook.execute()
            except: pass
        if img: os.remove(f'{basepath}/out/post.jpg')
        if thumb: os.remove(f'{basepath}/out/thumb.jpg')
    except Exception as e: print('postwebhook err - ' + str(e))


@tasks.loop(minutes=1440)
async def server_roles():
    try:
        now = datetime.datetime.now(tz_NP)
        last_day = calendar.monthrange(now.year, now.month)[1]
        if now.day == last_day:
            for memb in topmemrole.members:
                await memb.remove_roles(topmemrole)
            for memb in voterole.members:
                await memb.remove_roles(voterole)
            for role in support_server.roles:
                if role.id not in defautroles:
                    if role.is_assignable():
                        await role.delete()
        else:
            voters = []
            voterlist = requests.get("https://top.gg/api/bots/786534057437691914/votes",
                                     headers={"Authorization": os.getenv("TopGG_dbl_token")}, timeout=20).json()
            for voter in voterlist:
                voters.append(int(voter['id']))

            lbdata = {}
            players = requests.get('https://mee6.xyz/api/plugins/levels/leaderboard/785079398969507880').json()['players']
            for pl in players:
                lbdata[int(pl['id'])] = int(pl['level'])

            server_members = json.load(open(basepath + '/out/server_members.json'))

            top5serves=[]
            winner_servers = list(t_collection.find().sort('score', pymongo.DESCENDING))
            for server in winner_servers:
                top5serves.append(server['id'])

            memcnt={}
            async for member in support_server.fetch_members(limit=None):
                if member.bot: continue

                if member.id in voters: await member.add_roles(voterole)
                if not member.id in lbdata: continue
                lvl = lbdata[member.id]
                if lvl >= 100:
                    await member.add_roles(lvl100)
                    await member.remove_roles(lvlfifty)
                elif lvl >= 50:
                    await member.add_roles(lvlfifty)
                    await member.remove_roles(lvlthirty)
                elif lvl >= 30:
                    await member.add_roles(lvlthirty)
                    await member.remove_roles(lvltwenty)
                elif lvl >= 20:
                    await member.add_roles(lvltwenty)
                    await member.remove_roles(lvlfifteen)
                elif lvl >= 15:
                    await member.add_roles(lvlfifteen)
                    await member.remove_roles(lvlten)
                elif lvl >= 10:
                    await member.add_roles(lvlten)
                    await member.remove_roles(lvlfive)
                elif lvl >= 5:
                    await member.add_roles(lvlfive)
                    await member.remove_roles(newbie)

                if not str(member.id) in server_members.keys(): continue
                for guild in server_members[str(member.id)]:
                    gid=list(guild.keys())[0]
                    gname=guild[gid]
                    if int(gid) in top5serves: await member.add_roles(topmemrole)
                    role=discord.utils.get(support_server.roles, name=gname)
                    if role: await member.add_roles(role)
                    else:
                        if gid in memcnt:
                            if memcnt[gid]>=1:
                                try:
                                    role = await support_server.create_role(name=gname)
                                    await member.add_roles(role)
                                except: pass
                            else: memcnt[gid]+=1
                        else: memcnt[gid]=1
        async for entry in support_server.bans(limit=None):
            await support_server.unban(entry.user)
    except Exception as e: print('server_roles err - ' + str(e))

def ronb_post_url(id):
    return f"https://www.facebook.com/officialroutineofnepalbanda/posts/{id}"

first_run = True
@tasks.loop(minutes=15)
async def ronb_fb():
    try:
        global lastone
        tme=[]
        with open(basepath+'/out/'+choice(['c1.json','c2.json','c3.json']), 'r') as file:
            _scraper.mbasic_headers = json.load(file)        
        #set_user_agent("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/603.3.8 (KHTML, like Gecko) Version/10.1.2 Safari/603.3.8")
        for post in get_posts('officialroutineofnepalbanda',base_url="https://mbasic.facebook.com", pages=1): #start_url="https://mbasic.facebook.com/officialroutineofnepalbanda?v=timeline", pages=1): #, cookies=basepath+'/out/' +choice(['cook.txt','cookie.txt','cookies.txt'])):
            ptid = post['post_id']
            print(ptid)
            tme.append(ptid)
            global first_run
            if first_run: continue

            if ptid in lastone['ronb_fb']: continue
            lastone['ronb_fb'].append(ptid)
            pvalu = post['text'].replace('See more',' ')
            if(pvalu[:int(len(pvalu)/2)].strip()==pvalu[int(len(pvalu)/2):].strip()): pvalu=pvalu[int(len(pvalu)/2):].strip()
            pvalus=pvalu.lower()
            if '#stayupdated' in pvalus:
                r = json.loads(requests.get(os.getenv("Github_gist_URL")).json()['files']['blob.json']['content'])
                hooks = r['hooks']
                imgts=post['image'] # post['images'][0]
                if imgts is None: imgts=post['image_lowquality']
                #wget.download(imgts, out='post.jpg')
                with open('post.jpg', 'wb') as image: image.write(_scraper.get(imgts).content)
                #im = Image.open(rf"{basepath}/out/post.jpg")
                #w, h = im.size
                #if h == 1080: cropcordinate = 133
                #elif h == 526: cropcordinate = 0
                #elif h == 960: cropcordinate = 145
                #else: cropcordinate = 248
                #cropcordinate = 0
                #im.crop((0, 0, w, h - cropcordinate)).save(f'{basepath}/out/nine.jpg')
                webhooks = AsyncDiscordWebhook.create_batch(username='Routiney', avatar_url='https://i.imgur.com/tQOuMXT.png', content=pvalu,urls=hooks, rate_limit_retry=True)
                with open("post.jpg", "rb") as f:
                    for webhook in webhooks:
                        webhook.add_file(file=f.read(), filename='post.jpg')
                        try: await webhook.execute()
                        except: pass
                #os.remove('post.jpg')
                #os.remove(f'{basepath}/out/nine.jpg')
                continue
            matched=None

            if 'offer' in pvalus or '#ad' in pvalus or '#tvc' in pvalus or 'studyinfomercial' in pvalus or 'edupromo' in pvalus or '#promo' in pvalus or 'infomercial' in pvalus or 'infomerical' in pvalus or 'youtube.com' in pvalus or 'youtu.be' in pvalus or 'events.khalti.com' in pvalus:
                tosend='This is Most Probably a Sponsored Post. Click on Routine of Nepal banda above if you want to view the post'
                matched=True
            elif ('opportunity' in pvalus) and ('college' in pvalus or 'bit.ly' in pvalus):
                tosend = 'This is Most Probably a college Sponsored Post. Click on Routine of Nepal banda above if you want to view the post'
                matched=True
            elif ('scholarship' in pvalus) and ('entrance' in pvalus or 'admission' in pvalus or 'event' in pvalus or 'intake' in pvalus or 'bit.ly' in pvalus):
                tosend='This is Most Probably an Studyinfomercial Post. Click on Routine of Nepal banda above if you want to view the post'
                matched = True
            elif 'happy birthday' in pvalus:
                tosend = 'Falano ko Birthday re'
                matched=True
            elif 'anniversary' in pvalus:
                tosend='Something stupid happened years back on this day'
                matched=True
            elif ('fantasy' in pvalus) and ('ronb' in pvalus or 'we ' in pvalus):
                tosend = 'Yet another RONB Fantasy League Post'
                matched=True
            elif 'ronb' in pvalus:
                tosend = 'Filtered out a useless Post nobody asked for'
                matched=True
            elif '#goodnight' in pvalus:
                tosend='Good Night!'
                matched=True
            elif 'good morning' in pvalus:
                tosend='Good Morning!'
                matched=True

            if matched:
                embed = DiscordEmbed(description=tosend,color='e14044')
                embed.set_author(name='Routine of Nepal banda', url=post['post_url'], icon_url='https://i.ibb.co/jvY8NK2/ronb.png')
                await postwebhook(embed)
                continue
            try:
                if detect(pvalu) != 'en':
                    pvalulen=len(pvalu)
                    if pvalulen >= 500000000:
                        outp = [(pvalu[i:i + 500]) for i in range(0, pvalulen, 500)]
                        mat = len(outp)
                        pvalu = '.......' + outp[mat-1]
                        posz = 0
                        for m in range(0, (mat-1)):
                            if posz == 0:
                                embed = DiscordEmbed(description=outp[m] + '.......', color='e14044')
                                posz = 1
                            else:
                                embed = DiscordEmbed(description='.......' + outp[m] + '.......', color='e14044')
                            embed.set_author(name='Routine of Nepal banda', url=post['post_url'],icon_url='https://i.ibb.co/jvY8NK2/ronb.png')
                            await postwebhook(embed)
            except Exception as e:
                print(str(e))

            if any(substring in pvalus for substring in ["😢","💔",":(",":'(","emotion","cry","crie","death","dead","sad ","injured","accident"]): x=None
            else: x='x'
            if post['video'] is not None:
                embed = DiscordEmbed(description=pvalu, color='e14044')
                embed.set_author(name='Routine of Nepal banda', url=post['post_url'], icon_url='https://i.ibb.co/jvY8NK2/ronb.png')
                embed.set_image(url='https://i.ibb.co/JRGtm4s/sry.png')
                await postwebhook(embed,footer=x)
                continue
            embed = DiscordEmbed(description=pvalu, color='e14044')
            embed.set_author(name='Routine of Nepal banda', url=post['post_url'], icon_url='https://i.ibb.co/jvY8NK2/ronb.png')
            imgcount = 0

            try:
                imgs = post['images']
                imgcount = len(imgs)
            except: pass
            if imgcount > 2:
                await postwebhook(embed)
                for img in imgs:
                    embed = DiscordEmbed(description='\u200b', color='e14044')
                    # embed.set_image(url=img)
                    await postwebhook(embed,img,footer=x)
                continue
            elif imgcount==2:
                # embed.set_image(url=imgs[1])
                # embed.set_thumbnail(url=imgs[0])
                await postwebhook(embed,imgs[1],imgs[0],footer=x)
            else:
                # embed.set_image(url=post['image'])
                imgts=post['image']
                if imgts is None: imgts=post['image_lowquality'] # post['images_lowquality'][0]
                if not '_nc_ht=' in imgts: imgts=None
                await postwebhook(embed,imgts,footer=x)
        if tme:
            first_run = False
            lastone['ronb_fb']=tme
    except Exception as e: print('ronb_fb err - ' + str(e))

#with open(basepath+'/apify_accounts.json', 'r') as file:
#    apify_accounts = json.load(file)
async def parse_apify():
    apify_user = choice(list(apify_accounts.keys()))
    apify_key = apify_accounts[apify_user]
    requests.get(f"https://api.apify.com/v2/actor-tasks/{apify_user}~facebook-posts-scraper-task/runs?token={apify_key}&method=POST")
    for i in range(5):
        check = requests.get(f"https://api.apify.com/v2/actor-tasks/{apify_user}~facebook-posts-scraper-task/runs/last?token={apify_key}").json()
        if check["data"]["status"] == "SUCCEEDED": break
        if i==4: return []
        await asyncio.sleep(30)
    posts= requests.get(f"https://api.apify.com/v2/actor-tasks/{apify_user}~facebook-posts-scraper-task/runs/last/dataset/items?token={apify_key}").json()
    for post in posts:
        post['post_url']=ronb_post_url(post['postId'])
        images=[]
        has_video=False
        if 'media' in post:
            for m in post['media']:
                if m['__isMedia']=='Photo': images.append(m['photo_image']['uri'])
                else: has_video=True
        post['images']=images
        post['has_video']=has_video
    return posts

async def parse_rssground():
    posts=[]

    url="https://www.rssground.com/services/facebook-rss/66cde8da34fbd"
    r=requests.get(url)
    if r.status_code!=200:
        return posts
    xml_string=r.text

    root = ET.fromstring(xml_string)
    channel = root.find('channel')
    items = channel.findall('item')

    for item in items:
        post={}
        post['has_video']=False
        post['images']=[]

        postId=item.find('link').text.split('_')[-1]
        post['postId']=postId
        text=item.find('description').text
        if not text: text=""
        post['text']=html.unescape(text.replace('<br />', '').strip())
        post['post_url']=ronb_post_url(postId)

        namespace = {'media': 'http://search.yahoo.com/mrss/'}
        images=item.findall('.//media:content', namespaces=namespace)
        for image in images:
            post['images'].append(image.get('url'))

        posts.append(post)

    return posts


@tasks.loop(minutes=15)
async def third_party_ronb_fb():
    try:
        global lastone
        tme=[]
        posts = await parse_rssground()
        for post in posts[:10]:
            ptid = post['postId']
            print(ptid)
            tme.append(ptid)
            global first_run
            if first_run: continue

            if ptid in lastone['ronb_fb']: continue
            lastone['ronb_fb'].append(ptid)
            pvalu = post['text']
            pvalus=pvalu.lower()

            post_url=post['post_url']

            images=post['images']
            has_video=post['has_video']
            if '#stayupdated' in pvalus:
                r = json.loads(requests.get(os.getenv("Github_gist_URL")).json()['files']['blob.json']['content'])
                hooks = r['hooks']
                imgts=images[0]
                with open(f"{basepath}/out/post.jpg", 'wb') as image: image.write(_scraper.get(imgts).content)
                im = Image.open(rf"{basepath}/out/post.jpg")
                w, h = im.size
                if h == 1080: cropcordinate = 133
                elif h == 526: cropcordinate = 66
                elif h == 960: cropcordinate = 145
                else: cropcordinate = 248
                cropcordinate = 0
                im.crop((0, 0, w, h - cropcordinate)).save(f'{basepath}/out/nine.jpg')
                webhooks = AsyncDiscordWebhook.create_batch(username='Routiney', avatar_url='https://i.imgur.com/tQOuMXT.png', content="Time for RONB's extra dose of News. #StayUpdated",urls=hooks, rate_limit_retry=True)
                with open(f"{basepath}/out/nine.jpg", "rb") as f:
                    file_content = f.read()
                    for webhook in webhooks:
                        if file_content:
                            webhook.add_file(file=file_content, filename='nine.jpg')
                        else:
                            embed = DiscordEmbed(description="\u200b",color='e14044')
                            embed.set_author(name='Routine of Nepal banda', url=post_url, icon_url='https://i.ibb.co/jvY8NK2/ronb.png')
                            with open(basepath + '/out/nine.jpg', "rb") as f:
                                webhook.add_file(file=f.read(), filename='nine.jpg')
                            embed.set_image(url="attachment://nine.jpg")
                            webhook.add_embed(embed)

                        try: await webhook.execute()
                        except: pass
                #os.remove(f"{basepath}/out/post.jpg")
                #os.remove(f'{basepath}/out/nine.jpg')
                continue
            matched=None

            if 'offer' in pvalus or '#collab' in pvalus or '#collabad' in pvalus or '#ad' in pvalus or '#tvc' in pvalus or 'studyinfomercial' in pvalus or 'edupromo' in pvalus or '#promo' in pvalus or 'infomercial' in pvalus or 'infomerical' in pvalus or 'youtube.com' in pvalus or 'youtu.be' in pvalus or 'events.khalti.com' in pvalus:
                tosend='This is Most Probably a Sponsored Post. Click on Routine of Nepal banda above if you want to view the post'
                matched=True
            elif ('opportunity' in pvalus) and ('college' in pvalus or 'bit.ly' in pvalus):
                tosend = 'This is Most Probably a college Sponsored Post. Click on Routine of Nepal banda above if you want to view the post'
                matched=True
            elif ('scholarship' in pvalus) and ('entrance' in pvalus or 'admission' in pvalus or 'event' in pvalus or 'intake' in pvalus or 'bit.ly' in pvalus):
                tosend='This is Most Probably an Studyinfomercial Post. Click on Routine of Nepal banda above if you want to view the post'
                matched = True
            elif 'happy birthday' in pvalus:
                tosend = 'Falano ko Birthday re'
                matched=True
            elif 'anniversary' in pvalus:
                tosend='Something stupid happened years back on this day'
                matched=True
            elif ('fantasy' in pvalus) and ('ronb' in pvalus or 'we ' in pvalus):
                tosend = 'Yet another RONB Fantasy League Post'
                matched=True
            elif 'ronb' in pvalus:
                tosend = 'Filtered out a useless Post nobody asked for'
                matched=True
            elif '#goodnight' in pvalus:
                tosend='Good Night!'
                matched=True
            elif 'good morning' in pvalus:
                tosend='Good Morning!'
                matched=True

            if matched:
                embed = DiscordEmbed(description=tosend,color='e14044')
                embed.set_author(name='Routine of Nepal banda', url=post_url, icon_url='https://i.ibb.co/jvY8NK2/ronb.png')
                await postwebhook(embed)
                continue
            try:
                if detect(pvalu) != 'en':
                    pvalulen=len(pvalu)
                    if pvalulen >= 500000000:
                        outp = [(pvalu[i:i + 500]) for i in range(0, pvalulen, 500)]
                        mat = len(outp)
                        pvalu = '.......' + outp[mat-1]
                        posz = 0
                        for m in range(0, (mat-1)):
                            if posz == 0:
                                embed = DiscordEmbed(description=outp[m] + '.......', color='e14044')
                                posz = 1
                            else:
                                embed = DiscordEmbed(description='.......' + outp[m] + '.......', color='e14044')
                            embed.set_author(name='Routine of Nepal banda', url=post_url,icon_url='https://i.ibb.co/jvY8NK2/ronb.png')
                            await postwebhook(embed)
            except Exception as e:
                print(str(e))

            if any(substring in pvalus for substring in ["😢","💔",":(",":'(","emotion","cry","crie","death","dead","sad ","injured","accident"]): x=None
            else: x='x'
            if has_video:
                embed = DiscordEmbed(description=pvalu, color='e14044')
                embed.set_author(name='Routine of Nepal banda', url=post_url, icon_url='https://i.ibb.co/jvY8NK2/ronb.png')
                embed.set_image(url='https://i.ibb.co/JRGtm4s/sry.png')
                await postwebhook(embed,footer=x)
                continue
            embed = DiscordEmbed(description=pvalu, color='e14044')
            embed.set_author(name='Routine of Nepal banda', url=post_url, icon_url='https://i.ibb.co/jvY8NK2/ronb.png')
            imgcount = 0

            imgcount = len(images)
            if imgcount > 2:
                await postwebhook(embed)
                for img in images:
                    embed = DiscordEmbed(description='\u200b', color='e14044')
                    await postwebhook(embed,img,footer=x)
                continue
            elif imgcount==2:
                await postwebhook(embed,images[1],images[0],footer=x)
            elif images:
                imgts=images[0]
                await postwebhook(embed,imgts,footer=x)
            else:
                await postwebhook(embed,footer=x)
        if tme:
            first_run = False
            lastone['ronb_fb']=tme
    except Exception as e: 
        print('third_ronb_fb err - ' + str(e))
        if len(tme)>1: 
            tme.pop()
            lastone['ronb_fb'].extend(tme)


@tasks.loop(minutes=20)
async def aawaajnews():
    try:
        global lastone
        tme=[]
        medias = cl.user_medias(6644348287, 2)
        for post in medias:
            tme.append(post.pk)
            if post.pk in lastone['aawaajnews']: continue
            if not post.media_type==1:continue
            lastone['aawaajnews']=post.pk
            embed = DiscordEmbed(description=post.caption_text.split('\n')[0], color='e14044')
            embed.set_author(name='aawaajnews', url='https://www.instagram.com/p/'+post.code, icon_url='https://i.ibb.co/nBk20Zv/image.png')
            await postwebhook(embed, str(post.thumbnail_url), footer='x')
            # like_count
        if tme:
            lastone['aawaajnews'] = tme
    except Exception as e: print('aawaajnews err - ' + str(e))


@tasks.loop(minutes=5)
async def setopati():
    try:
        global lastone
        tme=[]
        response = requests.get('https://en.setopati.com/feed')
        trees = ET.fromstring(response.content)
        for item in trees.findall('.//item')[:2]:
            tit=item.find('./title').text.strip()
            desc=item.find('./description').text.strip()
            link=item.find('./link').text.strip()
            if any(keyword in link for keyword in ['blog','political']):continue

            tme.append(link)
            if link in lastone['setopati']: return
            lastone['setopati'].append(link)

            r=requests.get(link)
            img=re.findall('https?://.*?(?=")',re.findall('property="og:image".*',r.text)[0])[0]
            embed = DiscordEmbed(title=tit, description=desc, color=0x2596be)
            embed.set_author(name='Setopati', url=link,icon_url='https://en.setopati.com/uploads/authors/15691057161525672068logo.jpg')
            embed.set_image(url=img)
            await postwebhook(embed, footer='x')
        if tme:
            lastone['setopati'] = tme
    except Exception as e: print('setopati err - ' + str(e))


@tasks.loop(minutes=5)
async def onlinekhabar():
    try:
        global lastone
        tme = []
        response = requests.get('https://english.onlinekhabar.com/feed')
        trees = ET.fromstring(response.content)
        for item in trees.findall('.//item')[:3]:
            link= item.find('./link').text.strip()
            tme.append(link)
            if link in lastone['onlinekhabar']: return
            lastone['onlinekhabar'].append(link)
            cats= item.findall('./category')
            cont=0
            for cat in cats:
                if cat.text.lower() in ['infotainment','cover','politics','gadget','accident','travel','visit']: cont=1
            if cont: continue
            ns = {'dc': 'http://purl.org/dc/elements/1.1/','content': 'http://purl.org/rss/1.0/modules/content/'}
            content=item.find('./content:encoded', ns).text
            text = content.split('\n\n\n')[1].strip()
            desc=re.sub('<.*?>', '', text)
            if len(desc)<30:
                text = content.split('\n\n\n')[2].strip()
                desc = re.sub('<.*?>', '', text)
            img=re.findall('https?://english.onlinekhabar.com/wp-content/uploads/.*?(?=")',content)[0]
            embed = DiscordEmbed(description=desc, color=0x2596be)
            embed.set_author(name='OnlineKhabar', url=link,icon_url='https://english.onlinekhabar.com/wp-content/themes/onlinekhabar-english-2020/img/author-img.png')
            embed.set_image(url=img)
            await postwebhook(embed, footer='x')
        if tme:
            lastone['onlinekhabar'] = tme
    except Exception as e: print('onlinekhabar err - ' + str(e))

@bot.event
async def on_ready() -> None:
    await bot.change_presence(activity=discord.Activity(type=discord.ActivityType.watching, name="over Gedah"))
    print(f'Logged in as {bot.user} (ID: {bot.user.id})')
    print('------')

    await bot.tree.sync()

    global support_server,topmemrole,voterole,newbie,lvlfive,lvlten,lvlfifteen,lvltwenty,lvlthirty,lvlfifty,lvl100
    support_server = bot.get_guild(785079398969507880)
    topmemrole=support_server.get_role(871224668789559346)
    voterole=support_server.get_role(869263866054467584)
    newbie=support_server.get_role(lvlroles[0])
    lvlfive=support_server.get_role(lvlroles[1])
    lvlten=support_server.get_role(lvlroles[2])
    lvlfifteen=support_server.get_role(lvlroles[3])
    lvltwenty=support_server.get_role(lvlroles[4])
    lvlthirty=support_server.get_role(lvlroles[5])
    lvlfifty=support_server.get_role(lvlroles[6])
    lvl100=support_server.get_role(lvlroles[7])
    server_roles.start()
    arguments = sys.argv[1:]

    global first_run
    if '0' in arguments: first_run = False 
    if '1' in arguments:
        third_party_ronb_fb.start()
    if '2' in arguments:
        aawaajnews.start()
    if '3' in arguments:
        setopati.start()
    if '4' in arguments:
        onlinekhabar.start()

@bot.event
async def on_message(message):
    if message.content=="!roll" and message.author.id==736529187724197951:
        guild = message.guild
        if guild:
            members = guild.members
            while True:
                chosen_one = choice(members)
                if chosen_one.bot:continue
                await message.channel.send(f"<@{chosen_one.id}>")
                break


    if message.channel.id==1208706973621686352 and message.author.id==736529187724197951:
        try:
            r=requests.post(f"https://api.telegram.org/bot{os.getenv('tg_k_bot_token')}/sendMessage", data={'chat_id': os.getenv('tg_k_chat_id'), 'text': message.content})
            if r.status_code==200:
                await message.add_reaction('✅')
            else: await message.channel.send(str(r.text))
        except Exception as e:
            await message.channel.send(str(e))
        
        try:
            premsg= json.loads(open(ftpmsgfilename).read())
            
            if premsg['reply2'].strip()!='': premsg['reply1']=premsg['reply2']
            premsg['reply2']=message.content

            with open(ftpmsgfilename,'w') as f:
                json.dump(premsg,f)
                
            await message.add_reaction('👌')
        except Exception as e:
            await message.channel.send(str(e))

bot.run(os.getenv("second_bot_token"))
