import vars

import os
from dotenv import load_dotenv
load_dotenv()

vars.basepath =  os.path.dirname(os.path.abspath(__file__))

import discord
from discord.ext.commands import Bot, Context

intents = discord.Intents.default()
intents.members = True
intents.guilds = True
intents.message_content = True

import asyncio

import traceback
from datetime import datetime

from cogs.checks import guard_uncle

DISCORD_BOT_TOKEN = os.getenv("DISCORD_BOT_TOKEN")

bot = Bot(command_prefix='r!', intents=intents) # , chunk_guilds_at_startup=False)
bot.remove_command('help')

from db import Database
bot.db = Database()
@bot.event
async def on_ready() -> None:
    print(f'Logged in as {bot.user} (ID: {bot.user.id})')
    print('------')


@bot.check
async def cmd_checks(ctx):
    return await guard_uncle(ctx)


@bot.event
async def on_error(event, *args, **kwargs):
    embed = discord.Embed(title=':x: Event Error', colour=0xe74c3c)
    embed.add_field(name='Event', value=event)
    embed.description = '```py\n%s\n```' % traceback.format_exc()
    embed.timestamp = datetime.utcnow()
    bot.AppInfo = await bot.application_info()
    await bot.AppInfo.owner.send(embed=embed)


async def load_cogs() -> None:
    for file in os.listdir(f"{os.path.realpath(os.path.dirname(__file__))}/cogs"):
        if file.endswith(".py"):
            extension = file[:-3]
            try:
                await bot.load_extension(f"cogs.{extension}")
            except Exception as e:
                exception = f"{type(e).__name__}: {e}"
                print(f"Failed to load extension {extension}\n{exception}")
asyncio.run(load_cogs())


async def setup_hook():
    await bot.tree.sync()
bot.setup_hook = setup_hook

if __name__ == "__main__":
    bot.run(DISCORD_BOT_TOKEN)
