import os
from dotenv import load_dotenv
import pymongo

class Database:
    _instance = None
    _initialized = False

    def __new__(cls):
        if cls._instance is None:
            cls._instance = super(Database, cls).__new__(cls)
        return cls._instance

    def __init__(self):
        if not self._initialized:
            load_dotenv()
            self.client = pymongo.MongoClient(f'mongodb://{os.getenv("mongodb_creds")}/')
            self.lb_db = self.client.lb
            self.mim_db = self.client.mim
            self.collections = {
                's': self.lb_db['servers'],
                't': self.lb_db['top5'],
                'u': self.mim_db['users'],
                'r': self.mim_db['reacts'],
            }
            Database._initialized = True

    @property
    def s_collection(self):
        return self.collections['s']

    @property
    def t_collection(self):
        return self.collections['t']
    
    @property
    def u_collection(self):
        return self.collections['u']

    @property
    def r_collection(self):
        return self.collections['r']    
