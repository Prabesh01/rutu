Links: [@_prabesh](https://discordapp.com/users/736529187724197951) • [Add Bot](https://discord.com/api/oauth2/authorize?client_id=786534057437691914&permissions=2687888449&scope=bot%20applications.commands) • [Support Server](https://discord.gg/ehRSPHuTDy)

---
This is the source code of [Routiney Discord Bot](https://top.gg/bot/786534057437691914#/). After almost 2 years of running outdated code and 4 years of using utter pathetic code, I finally got time to Migrate it to discord.py v2. Tried my best to write organized codes instead of freaking 3k+ messy lines in a single file as you can see in [old.bot.py](https://gitlab.com/Prabesh01/rutu/-/blob/master/old.bot.py).

This exact code base *(excluding data/***, db and .env secrets)* is being used in my VPS to host Routiney.

___

## Self-host
*Take this code base as a reference if you are curious how certain command works BTS. I won't be providing any support for self-hosting. You are welcome to go ahead and try to do it yourself.*

## Overview
### Data
_Data are stored in 3 different ways as per convenience_
- gist.github.com: 
  - blob.json (r!sub data)
    - `{"hooks": ["webhook_url", ...],"channel": ["channel_id", ...]}`
  - rnews.json (r!news data)
    - `{"server_id": channel_id, ...}`
  - blobre.json (r!repost data)
    - `{"server_id": "webhook_url", ...}`
  - bloboa.json (r!oa data)
    - `{"oa": ["server_id", ...]}`

- mongodb:
  - client = pymongo.MongoClient()
  - mimdb = client.mim
  - lbdb = client.lb
  - mim_uploads = mimdb['users']    
    - uid(int), mimid(str), date(datetime.datetime)
  - mim_reacts = mimdb['reacts']  
    - uid(int), mimid(str), react(1/-1)
  - leaderboad_servers = lbdb['servers'] 
    - id(int), serverName(str), score(int)
  - top5_servers = lbdb['top5'] 
    - id(int), name(str), score(int), invite(str)
- local file:
  - r!event data: [/data/evz.json](https://gitlab.com/Prabesh01/rutu/-/blob/master/data/evz.json?ref_type=heads)
  - mim uploads banlist: [/data/mimban.json](https://gitlab.com/Prabesh01/rutu/-/blob/master/data/mimban.json?ref_type=heads)
  - mim uploads: [/data/mim/](https://gitlab.com/Prabesh01/rutu/-/tree/master/data/mim?ref_type=heads)
  - custom short events like birthdays to be shown in `t` and `cal` command: [/data/events/](https://gitlab.com/Prabesh01/rutu/-/tree/master/data/events?ref_type=heads) 
### Scripts
_Output of these scripts are kept in [/out/](https://gitlab.com/Prabesh01/rutu/-/tree/master/out?ref_type=heads) dir_<br><br>
_Cronjob time is in UTC. Basic Format: `* * * * * python3 -u ~/rutu/scripts/X.py > ~/botlogs/x.log 2>&1 &`_
- 19.py: [Command Depreciated!]
  - Script for `19` command. 
  - Fetches covid updates from [portal.edcd.gov.np](https://portal.edcd.gov.np/covid19/) and fills the data on the template [covid.png](https://gitlab.com/Prabesh01/rutu/-/raw/master/assets/covid.png?ref_type=heads)
  - Runs everyday at 6 PM and 9 PM NPT: `15 12 * * *` & `15 15 * * *`
- cal.py:
  - Script for `cal` command.
  - Takes screenshot of [hamropatro.com](https://www.hamropatro.com/) using [Browshot](https://browshot.com/)'s API. It allows me to use custom javascript code on the site (to remove unwanted elements) before taking screenshot. After screenshot, crops the image.
  - Runs everyday at 12:01 am NPT: `16 18 * * * `
- xutti.py
  - Script for `bida` command.
  - Fetches current year's public holiday data from [hamropatro](https://www.hamropatro.com/nepali-public-holidays).
  - Running it once a year at first day (Baishakh 1) will do. But better to run first day of each nepali month to not miss changes.
- miti.py
  - Script for `/cal search` command.
  - Fetches all the events of current year from [Miti - The Nepali Calendar](https://miti.bikram.io/)
  - Running it ... same as above
- server.bot.py<br>
_As this file is not part of main bot's code, If you are trying to self-host, you can ignore this file and also remove `server_members_fetch()` from [cogs/tasks.py](https://gitlab.com/Prabesh01/rutu/-/blob/master/cogs/tasks.py?ref_type=heads#L8)._ This is code for my Server Management Bot. It does the following:
  - Script for `r!sub`. Scrapes RONB's facebook ~~or twitter~~ posts, filters ads and sends to subscribed channels. Since the news is posted to webhooks, no bot account is needed. Or any bot other than the main bot can be used. Pretty convinient as this avoids main bot getting rate limited. To use this, you need to save your facebook's cookies in netscape format in `out/cook.txt`
  - After the main bot [exports](https://gitlab.com/Prabesh01/rutu/-/blob/master/cogs/tasks.py?ref_type=heads#L142) mutual servers with all members in the server to `out/server_members.json`, if the member is in any of the top5 server, it gives them a special role.
  - It creates roles with name of the servers s/he is on and give it to them.
  - Since mee6 level roles is a premium feature, this bot fetches user's levels from public leaderboard and assign respective level roles to the members.
  - Gives a special role to all those who have voted the main bot in [top.gg](https://top.gg/bot/786534057437691914/vote).
  - Clears server's banlist cuz no one deserves to be banned from my shitty server.
- play.py
  - Script for `/play` command
  - Creates data/play.json
  - No need to run since the json file is already available.
- ss.py
  -  Gives value for ss variable of vars.py. The variable is used by cogs/news.py which is
     - Script for `r!news`
     - Gets news from [hamropatro](https://hamropatro.com/news) and sends to subscribed channels.
     - runs every 2 min: `@tasks.loop(minutes=2)`
- over.py
    - Removes invalid webhooks from [blob.json](https://gitlab.com/Prabesh01/rutu#data) and makes sure only one channel is subscribed per channel.
    - runs once a month: `0 0 1 * *`
- backup.py
  - Copies .env and crontab file to [/data/](https://gitlab.com/Prabesh01/rutu/-/tree/master/data?ref_type=heads)
  - Exports mongodb databases to the same dir.
  - Backs up json data from gist.github.com.
  - Zips the data directory and uploads the zip file to file.io. Then deletes the zip file. Finally, Sends the file link to discord webhook.
  - Runs once a week: `0 0 * * 0`
  - To restore, `mongorestore -d mim data/mongo/mim/` & `mongorestore -d lb data/mongo/lb/`

### Quick notes to self:
- `mongosh` `show dbs` `use mim` `db.dropDatabase()`
- `show tables` `db.users.findOne({"mimid": ''})`
- `sudo apt install ffmpeg zip`
- `python3 -m http.server --directory ~/rutu/data/mim/`
- `cl.login()`
- [4.4](https://www.mongodb.com/docs/v4.4/tutorial/install-mongodb-on-ubuntu/)

## License

[![CC0](https://i.creativecommons.org/l/by-nc/4.0/88x31.png)](http://creativecommons.org/licenses/by-nc/4.0/)
