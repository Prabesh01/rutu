import vars
from cogs.checks import guard_uncle
from db import Database

import asyncio
import traceback
import datetime
import os
import sys
arguments = sys.argv[1:]

import discord
from discord.ext.commands import Bot
from dotenv import load_dotenv
load_dotenv()

vars.basepath =  os.path.dirname(os.path.abspath(__file__))
DISCORD_BOT_TOKEN = os.getenv("DISCORD_BOT_TOKEN")


class RoutineyBot(Bot):
    
    def __init__(self):
        intents = discord.Intents.default()
        intents.members = True
        intents.guilds = True
        intents.message_content = True
        
        super().__init__(command_prefix='r!', intents=intents, chunk_guilds_at_startup=False if 'test' in arguments else True)
        self.remove_command('help')
        self.db=Database()
        self.AppInfo = None


    async def get_app_info(self) -> None:
        """Cache application info."""
        if not self.AppInfo:
            self.AppInfo = await self.application_info()


    async def setup_hook(self) -> None:
        await self.load_all_cogs()
        if 'sync' in arguments: await self.tree.sync()
        await self.get_app_info()


    async def load_all_cogs(self) -> None:
        for file in os.listdir(f"{os.path.realpath(os.path.dirname(__file__))}/cogs"):
            if file.endswith(".py"):
                extension = file[:-3]
                try:
                    await self.load_extension(f"cogs.{extension}")
                except Exception as e:
                    exception = f"{type(e).__name__}: {e}"
                    print(f"Failed to load extension {extension}\n{exception}")


    async def on_ready(self) -> None:
        print(f'Logged in as {self.user} (ID: {self.user.id})')
        print('------')


    async def on_error(self, event, *args, **kwargs):
        embed = discord.Embed(title=':x: Event Error', colour=0xe74c3c)
        embed.add_field(name='Event', value=event)
        embed.description = '```py\n%s\n```' % traceback.format_exc()
        embed.timestamp = datetime.datetime.now(datetime.timezone.utc)
        
        await self.AppInfo.owner.send(embed=embed)


async def initialize_bot() -> None:
    bot = RoutineyBot()
    
    @bot.check
    async def cmd_checks(ctx):
        return await guard_uncle(ctx)
    
    await bot.start(DISCORD_BOT_TOKEN)


def main():
    asyncio.run(initialize_bot())


if __name__ == "__main__":
    main()
